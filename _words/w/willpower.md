---
title: willpower
also-see:
    - self-control
    - self-restraint
---
_(willpower is a muscle - the more you exercise it, the more it grows, the better for it, for you, for humanity.)_

- the ability to control yourself
- strong determination that allows you to do something difficult (such as to lose weight or quit smoking)