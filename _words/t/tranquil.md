---
title: tranquil
---
_(be the one others see as "oddly tranquil" because you should've been nothing but upset given the circumstance, but you choose tranquility. Over time, then, tranquilness just comes natch to you.)_

* free from agitation of mind or spirit
* quiet, free from disturbance or turmoil, stable, peaceful
* untroubled and untroubling spirit

_(you should also have a tranquil self-assurance and be fazed by nothing.)_