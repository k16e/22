---
title: teleology
---
_(be teleological (in thinking))_

- believe that nature's designs, which every part of nature is, are on purpose; that everything is shaped by a purpose. Don't you agree?

_(Then, find for each thing its purpose, especially if you engage that thing.)_