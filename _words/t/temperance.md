---
title: temperance
---
_(be temperant, have temperance, exercise temperance in all things)_

- you have temperance when you make a habit of moderating your indulgence of the appetites and passions, never too much, just enough.
- temperance - be able to control your desires. Who said you couldn't?