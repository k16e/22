---
title: calm
---
_(two things to know about calm is that you should both cultivate it and keep it)_

* a quiet and peaceful state or condition
* a peaceful mental or emotional state

The thing about calm is taking control how stimuli (no matter their extreme - of rage or bliss) affect your mental and/or emotional state. Taking control not to let your [inner peace]({{site.url}}/words/peace/) be disrupted. Thus, outwardly showing calm despite.

To me, [calm is another word for 22]({{site.url}}/calm/), defining the practice in terms of it's ultimate why. I'm not sure the world has any more problem if calm is mastered individually. I urge you, learn calm and master it.