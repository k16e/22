---
title: composure
---
_(more than anything else, keep your composure)_

- a calmness or [repose]({{site.url}}/words/repose/) especially of mind, bearing, or appearance
- self-possession
- let all the distresses come, but your calmness of mind, your bearing, your appearance - your composure - remains intact. That's composure.

Let people be able to say of you, "she never loses her composure," that is, you're always calm.