---
title: compassion
also-see:
    - calm
---
_(because compassion satisfies an authentic desire to help, it can bring calm to the helper. Therefore, practice compassion daily.)_

- a feeling of wanting to help someone who is sick, hungry, in trouble, etc.
- sympathetic consciousness of others' distress together with a desire to alleviate it

Helping others genuinely, not in anticipation of reward, no strings attached, is calming. It's what compassion is and why you should make it a daily practice. Practice, because, while it's a natural trait, fending for oneself and the hustles of everyday life can too easily overwhelm, if not deplete, compassion. 

So, like in the definition, compassion requires conscious efforts along with a getting-used-to.