---
title: collected
---
_(stay collected, be aware you are and should be because it requires effort, concentrated effort)_

- calm and in control of your emotions
- possessed of calmness and composure often through concentrated effort

You stay collected by making a conscious effort to keep your faculties in [the middle]({{site.url}}/middle/), your emotions under total control, despite turbulence. It's like an ideal place to be from long-term practice of calm with 22. I believe over time, it becomes effortless - you're just collected.

Notice the use of "possessed" in the second expression? It's like you cannot not be calm anymore - it's that complete. 