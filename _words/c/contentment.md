---
title: contentment
---
_(Contentment is the most important ingredient of happiness, if not happiness itself.)_

* being satisfied, ok with what you have, who/where you are
* being happy because you are

I've added my take on contentment in the second definition. I don't want you to think that "showing satisfaction for one's possessions, status, or situation" implies having a certain level of wealth or position/regard in society/social circles to be contented. Contentment should be the means, not the end.

Contentment should be a choice, which if you can [breathe]({{site.url}}/one-breath) you can make. Set and achieve goals, improve in life, go get that, yeah, but keep contented at every level - with or without the upgrade. So, radiate contentment and [bring more peace]({{site.url}}/words/peace) into your life. 