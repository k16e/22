---
title: contemplative
---
_(always find time to be contemplative; cherish your contemnplative moods)_

* involving, allowing, or causing deep thought
* being quiet and thoughtful, regarding a thing steadily, as if to be lost in it

_(If you're contemplative enough, you'll hardly stumble. Yet, when you do, being contemplative of your human nature will let you easily forgive yourself, make changes, and move on.)_