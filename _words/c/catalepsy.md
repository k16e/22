---
title: catalepsy
---
_(go into catalepsy, you can be cataleptic at 22)_

- trancelike state where you lose every voluntary motion such that your limbs remain in whatever position you have them completely motionless.