---
title: carefree
---
_I believe a carefree life (or heart) is a most healthy one_

* having no worries or problems or troubles
* free from care

Carefree isn't irresponsible. It isn't absense of troubles, problems, challenges in life. But it is that, while you may have no control over these things that cause worry, you live without letting the worry get you - you live care free.