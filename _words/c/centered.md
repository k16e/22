---
title: centered
---
_(be centered all the time and live a full life; approach everything from a place of centered balance)_

- emotionally healthy and calm
- emotionally stable and secure

You find your center, then feel and stay centered. 

Your center is where your body (sitting, lying, or standing) is in perfect symmetry within the surface supporting your stance, holding you in position. It's where your internal compass would point to true North, the needle not dangling, so to speak. At such a time, your mind too, or no, you have your mind under control, taking it where only you want, and it's [even]({{site.url}}/words/equanimity/). Say that again - even.

In your center, you're planted. Rooted. You're [grounded]({{site.url}}/words/grounded/). Awareness to self tells of the strength of your body, firm where it is, unscattered, unshifting...you are right where you are - steady, stable, calm. 

Your center is where you always come back to whenever you sense a feeling of overwhelm, of things going out of control, of you losing it...anytime, return to your center - "recenter yourself in the present moment".