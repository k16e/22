---
title: self-possession
also-see:
    - self-control
---
_(If you got self-possession you cannot overreact or be overwhelmed by [the extremes of emotions]({{site.url}}/middle/))_

- control of one's emotions or reactions especially when under stress
- [composure]({{site.url}}/words/composure/)

Yes, self-possession is your only true possession. It's only your self you can control. Neither the outcome of the weather or of people's behavior can you control nor worry about. Therefore, only reason and decide how and when and why you should respond to these outcomes.

Money, power, fame, pleasures, friends, you name it, are all subject to change and loss and do get lost from our possessing them. Here now, good, then gone again, sometimes, even for good. But what remains? The self. You. You remain. So, possess what remains - you, yourself. 

Have self-possession, live a full life.