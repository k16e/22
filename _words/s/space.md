---
title: space
also-see:
    - solitude
---
_(with an app for everything these days, it's tough to allow empty space in our lives.)_

- the freedom and time to behave and think as you want to without being controlled or influenced by someone else [or something else]
- an opportunity for privacy or time to oneself

From social media to blogs to accomplishing set tasks/goals for the day to hanging out with friends & family to catching up on YouTube or Netflix to you name it, technology has so fueled our busyness we hardly have any space left in our lives each day. No, we shouldn't live this way.

I could say one of reasons I do 22 is just for that - space. To have more space in my life. More space for nothing, more space to get in touch with myself, my fears, my joys, to really feel the present and [just be]({{site.url}}/be/).

And space is freedom - you don't fear the dark, you don't fear uncertainty, you don't fear [solitude]({{site.url}}/words/solitude/), you don't fear silence. You truly enjoy these freedoms and the deliciouness of the present moment of silent stillness, whenever you carve one out for yourself. 

My encouragement? Do more [nothing]({{site.url}}/thinklist/nothing/), with 22 - like, do less and fear nothing.