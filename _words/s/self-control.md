---
title: self-control
also-see:
    - self-possession
    - self-restraint
    - willpower
---
_(self-control is an ability, a critical skill you should learn, master, and yet always exercise.)_

- control over your feelings or actions
- restraint exercised over one's own impulses, emotions, or desires

Because you're alive and got the power to, you can do anything at anytime. But you know you should not - because not everything is suitable at anytime, each thing having its time, its place, its purpose. Self-control is how you can. Or you would be no better than goats that'll mate in the open path while grazing or take a crap as they walk along so.