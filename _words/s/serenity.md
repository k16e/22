---
title: serenity
---
_(let your own serenity calm those around you)_

- you got serenity if you're utterly calm and peaceful, yes, to such a degree that it's contagious. Be serene is what I said.