---
title: self-restraint
also-see:
    - self-control
    - self-possession
    - willpower
---

- control over your own actions or feelings that keeps you from doing things you want to do but should not do
- restraint exercised over one's own impulses, emotions, or desires