---
title: nonchalance
---
_(face whatever crowd you come across with nonchalance)_

- the quality of ease (mostly indifference or unconcern),
- but associated with 22 with the sense of [insouciance]({{site.url}}/words/insouciance/), being up for any challenge without letting it disrupt your calm,
- thus, I could say, "have a _nonchalant_ ease, but don't be _nonchalant_ about the situation" - be concerned, care, but not troubled...