---
title: kind
also-see:
    - gentle
    - forbearing
---
_(to be kind is to bring happiness to others, but that brings happiness to you too)_

- having or showing a gentle nature and a desire to help others
- wanting and liking to do good things and to bring happiness to others

"Color me kind" is a thing to say more of to ourselves, so we'd be more open to areas where we can help others. Because, being gentle, seeing to the needs of neighbors, rooting for the happiness of the more, is the best kind of "selfishness" - it's for our happy. Be colored kind.

For when you're colored kind, you'll see all kinds of opportunities to practice kindness - holding the door open for the next person, taking a photo of a colleague during their presentation when they were at their best and sending it to them, holding on a genuine smile, giving up your spot in the queue for an elderly one, giving honest feedback/compliments, yes, many more.

Yes, being kind is for our happy - a way to be [happy]({{site.url}}/happy/).