---
title: unruffled
---
_(You choose to remain unruffled come what may)_

* not upset or disturbed
* [poised]({{site.url}}/words/poised/) and serene especially in the face of setbacks or confusion

Phew, this is a beautiful 22 word for me! It's a deliberate choice - no matter what, I'll remain poised, serene, **unruffled.**