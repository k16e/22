---
title: undaunted
also-see:
    - undaunted
    - unfazed
---
_(to be great at your choice of what to do/be, you must continue undaunted)_

- courageously resolute especially in the face of danger or difficulty
- not discouraged
- not afraid to continue doing something or trying to do something even though there are problems, dangers, etc.