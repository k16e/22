---
title: unfazed
also-see:
    - unruffled
    - undaunted
---
_(you make the decision to be unfazed no matter what life may present at any time)_

- not confused, worried, or shocked by something that has happened