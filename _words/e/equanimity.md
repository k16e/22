---
title: equanimity
---
_(nothing should ever disturb your equanimity, no, you can't be broken)_

- evenness of mind especially under stress

Let the world throw at you whatever it wants, you'll continue to [do what's happy]({{site.url}}/happy/), accepting whatever outcome with equanimity.