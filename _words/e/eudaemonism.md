---
title: eudaemonism
---
_(live eudamonically, live a eudamonic life, live happy, let the good spirits attend to you)_

- theory that the highest ethical goal is happiness and personal well-being. Ultimately, you will flourish if you live ethically. To flourish is happy.