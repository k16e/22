---
title: grounded
---
_(prepare to remain grounded anytime despite fame, praise, or admiration)_

* used to describe a person who is sensible and has a good understanding of what is really important in life
* mentally and emotionally stable
* admirably sensible, realistic, and unpretentious

Sensible, good understanding, and mental/emotional stability are key words and phrases to note here in the meaning/usage of _grounded_. These are states only a personal discipline like the practice of 22 can help you bring more of into your life. Being grounded is what you get from living [the middle way]({{site.url}}/middle).