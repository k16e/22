---
title: gentle
also-see:
    - forbearing
    - kind
---
_(a rich full life is gentle all the time, even when drastic actions must be taken)_

- having or showing a kind and quiet nature
- free from harshness, sternness, or violence