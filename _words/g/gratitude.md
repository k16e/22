---
title: gratitude
---
_(live each day with sincere gratitude)_

* a feeling of appreciation, of thanks
* the state of being grateful

Gratitude should be practiced to mastery because it makes happier and calmer. It leaves you free, [carefree]({{site.url}}/words/carefree).

Gratitude starts with awareness - find things to be grateful for. Goes on to perspective-shifting - there must be a positive trait in everything, every situation. And ends with humility and modesty - you're never too big to be helped, thus you're thankful to everyone everyday, for everyday.