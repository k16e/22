---
title: aplomb
---
_(you showed great aplomb dealing with the opposition; you should demonstrate perfect aplomb)_

- confidence, composure, poise, unfazed and unshaken self-assurance in spite of a situation.