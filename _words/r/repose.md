---
title: repose
---
_(you may repose sitting or lying; but your repose must be comfortable)_

- typically inactive state of a physical nature. If you were compared to a bayou, the difference would remain that the bayou at least moves slowly - at 22, you won't move at all! Don't fret, it's enjoyable.