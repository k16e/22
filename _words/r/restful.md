---
title: restful
also-see:
    - repose
---
_(by all means, seek places and do things that leave you restful.)_

- peaceful and quiet in a way that makes you relax
- marked by, affording, or suggesting rest and repose