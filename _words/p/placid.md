---
title: placid
also-see:
    - serenity
    - calm
---
_(develop and maintain a sunny, placid disposition for how else would you show you got your center?)_

- not easily upset or excited
- serenely free of interruption or disturbance
- calm and steady