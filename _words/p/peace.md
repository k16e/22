---
title: peace
---
_(bring more peace into your life and sustain it)_

* a state in which a person is not bothered by thoughts or feelings of doubt, guilt, worry, etc
* freedom from [negative emotions]({{site.url}}/negative)
* a state or position of perfect [tranquility]({{site.url}}/words/tranquil) and quiet

Inner peace is freedom - mental and emotional. It's a sweetness for mind, for body. This peace is a state of utmost calm that can be cultivated and reached by anyone desiring - we all should. I'd 10x pray and work for inner peace than world peace. Really, world peace is a consequence of inner peace.