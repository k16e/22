---
title: poised
---
_(Should calm govern you you would be **poised** both in emotions and in actions. Or, simply, you should be **poised**)_

* marked by balance or equilibrium
* having or showing a calm, confident manner

When you stand before an audience, do you radiate poise? Are you poised? Can you be? May [calm]({{site.url}}/calm/) get you there!