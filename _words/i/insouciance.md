---
title: insouciance
---
_(an almost childish insouciance is an asset for dealing with life's throws)_

* a relaxed and calm state, as if of a child who's ignorant of grievance of any situation.
* a feeling of not worrying about anything, of lighthearted unconcern

You may have a challenge, then you believe you can deal with it, so you remain insouciant. It's the charm of children, even if, for them, it's not that they know they can deal with it. I believe we all can commit to insouciance.