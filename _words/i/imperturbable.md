---
title: imperturbable
---
_(your imperturbability should begin from inside so that when it's noticed outward, it's not a show but a reflection of you)_

* marked by extreme calm and steadiness
* very hard to disturb or upset
* [serene]({{site.url}}/words/serenity/)

_(You've mastered calm when you can't be disturbed. Do note, though, that extreme calm is not impassivity, as suggested in Merriam-Webster. You do feel and take action - [do read more here]({{site.url}}/middle/).)_