---
title: introspect
---
_(introspect with 22, have several moments of quiet introspection everyday)_

- you're you looking into you, examining your thoughts and feelings, everyone of them, not judging or questioning them, just knowing they're there, have arisen, and will disappear after. Know yourself—thoroughly!