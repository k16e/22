---
title: What if I just want to sleep or have a nap during 22?
order: 5
---
Sleep, to me, is never something to resist. If anything, it is one thing we aren't having enough of. So, first, go ahead, use 22 to sleep or have a nap. But then, is it that easy? Well, my practice has been the [do nothing]({{site.url}}/thinklist/nothing/) technique. So, if you want to just sleep or have a nap with 22, "doing nothing" is your best bet. Enjoy.