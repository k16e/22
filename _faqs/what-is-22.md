---
title: What is 22?
order: 0
---
22 is lying or sitting still for 22 minutes. During the exercise, you don't move at all, you don't even scratch an itch. It's a discipline in stillness that brings calm and inner peace into your life. Do read more [here]({{site.url}}/what/).