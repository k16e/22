---
title: Why 22 minutes and/or why the number 22?
order: 1
---
The number 22 did come by pure accident - I just noticed I'd set the timer by that much for my third or so 22. And so I stuck with it. But then, I feel it coincides with a seemingly perfect timing for a practice like this. Here's what I mean:

- Typical naps are advised for 20 minutes or so
- Most health and fitness programs have a 21-day effectiveness span - the number...
- Typical daily calisthenics/aerobics is advised for roughly 22 minutes per day, that is, if you follow the 150 minutes per week moderate exercise recommendation
- Some believe that within 15-18 minutes of vigorous exercise you should've made the heart beat fast enough to achieve the purpose of a typical exercise session each day, which may also be why
- Most meditation sessions are timed at 15 minutes or so, and
- Actually, most meditations/yoga sessions go for the number 22.

Over time, though, no length of time would give a sweeter spot, nor does any other numeral repetition sound so natural, calming. 11? 33? 44? But 22? Well, you tell me.