---
title: Can 22 cure insomnia?
order: 4
---
Neither yes nor no. If I say yes, where's the science? And yet, if I say no, what about empirical evidences from people? And, not that I think that just doing 22 will get rid of insomnia and start making you sleep. I mean, insomnia can be symptomatic - you got to get to the root cause to get rid of it.

But 22 can engender the sort of calm that lets your body surrender to sleep and sleep, whether the mind likes it or not. [A lot more on sleep here.]({{site.url}}/themes/sleep-better/)