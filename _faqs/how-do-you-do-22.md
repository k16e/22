---
title: How do you do 22?
order: 2
---
All you need to do 22 are a prepared mind, a body [nonchalant]({{site.url}}/words/nonchalance/), and a chair or couch or mat or bed that can support your sitting or lying comfortably. It's really important to be comfortable and [centered]({{site.url}}/words/centered/) in the stance you take because you must stay in it for the rest of the 22 minutes. Do [read more here]({{site.url}}/how/).