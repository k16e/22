---
title: I cannot help but fall asleep during 22!
order: 6
---
As much as I encourage a lot of sleep, the time comes when you got to sit up and get [some thinking]({{site.url}}/thinklist/) done. At such a time, do 22 seated. [Lotus position](https://en.wikipedia.org/wiki/Lotus_position){:target="_blank" rel="noopener"} is fine. In case you can't hold your back straight long enough, avoid straining by leaning it against a wall or some other support.