---
title: Is 22 meditation?
order: 5
---
No. 22 is not meditation. But you can meditate with 22. By that I mean you can "focus on the breath", you can [think deeply]({{site.url}}/thinklist/) about and make sense of something - an object, a person, a people, or an action - or you can just [feel all the sensations]({{site.url}}/thinklist/scan/) you're getting from the present moment without judging any during a session.

But [22 itself]({{site.url}}/what/) is just that personal time you pick to bring calm and stillness to you. Then as a lifestyle, you become [more emotionally healthy]({{site.url}}/find-center/) and calm.