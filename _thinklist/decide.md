---
title: Decide action
---
Deciding action is pretty straightforward. It is, in a nutshell, announcing to yourself, “I’m going to do like this...”; or, “I’m going to be like this when I get there”. 

Because you have a set of morals or guiding principles you do not want to stray off from in your actions, “deciding action” can be a sure recipe to living [a deliberate, well-edited life]({{ site.url }}/nothing/). 

Consequently, you can tell you’re successful at this when you find yourself having fewer to no [actions to regret]({{ site.url }}/negative/) each morning.

### Technique & example
1.  Say, you’re invited to a party. What party is this?
2.  Who’s the host?
3.  Who all are attending?
4.  Why’re they attending?
5.  Why would you attend? Just because you’re invited? What role might you or should you or are you expected to play there?
6.  Will food be served? Drinks? Which ones? Would they agree with your diet? If not, what might you do?
7.  Can you even afford to attend, as in, can you pay your way there or participate monetarily if the need arises?

### It's practical
You’ll be asking and answering these sorts of questions during 22. 

Alternatively, you could use one session to formulate the questions. Think up all the questions that might possibly relate to this event, which requires good knowledge of the event. Then use another session to supply yourself answers.

{% include components/break.html %}

I’ve used “decide action” for staff general meetings at work, for managers’ meetings, for dates, even for dinner for just me. Definitely, this has wide-reaching applications in life.