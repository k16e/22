---
title: Understand something
---
Understanding something is knowing what it is or how it works or why it’s the way it is. Unlike [resolving something]({{site.url}}/thinklist/resolve), which asks for your conclusions and judgments, resolutions, your stand, understanding something is just to know it without questioning.

> Understanding is in the willingness to know. For "knowledge comes easily to those with understanding.” 
<cite>— Pro 14:6</cite>

It’s the difference between “why apples are good for you” and “why I don’t eat apples”. It’s the difference between translation and interpretation; between what she said and what you think she said. One looks at the thing, the other both the thing and where you or someone stands with it. 

Know that this is not _your understanding_ of something, but what that something _truly is_. 

Be careful here - don’t jump to conclusions, don’t be in a hurry to understand, to get to it. Don’t confirm, seek. Enjoy the process, dig deeper, deeper - it may never end.

### The technique
Pick a topic, a subject, something of interest, and come at it with what-, might-it-be-, and ah-let-me-see-type questions just to know what is inherent to the thing, not, never, what you think about it. What is native to this thing? Where’d it begin? How? Questions that help you look into it. 

You can take a stand later if you must, it’s up to you, but better if you understand it first. You may research and read about the thing pre-22. Then see if your new knowledge is aiding your understanding of it. Your [22 sessions]({{ site.url }}/how/) can be shared between coming up with the probing questions and thinking through researched/read facts. 

Constantly avoid taking sides - never what you think brandy is but what brandy is. Not why you love brandy and what you will do with it. But why brandy can be so loved or even hated because what it is.

### An example
What is brandy? You won’t end up here knowing whether or not I love brandy and why I take or never take brandy - do I take brandy? - but you’ll know what brandy is, thoroughly. That is, were brandy your subject of interest. What is brandy? How did it get its name? Who started it, where? Why? How did it catch on? What’s it do for people taking it? Why will it do that? How’s it still made? Are there published health facts about it? How do people like it? Why, _(without confirming any biases)_? 

The questions you come up with to understand something may differ from set to set depending on your subject. But they’ll only query what is inherent to the thing and get you to know it properly. What you do with that knowledge is entirely up to you.

{% include components/break.html %}

Understanding something does help in [resolving something]({{site.url}}/thinklist/resolve/) because knowledge precedes wisdom. But either practice can be applied to any subject differently.