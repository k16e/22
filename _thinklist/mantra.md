---
title: Do a mantra
---
You're either doing one thing that commands all your attention, that fully soaks you up, or your mind is running a million maze at once. Repeating a mantra for as long as you [want to be]({{site.url}}/be) is one way to keep still and help get the mind to [settle and focus]({{site.url}}/themes/increase-focus).

Compared to other ways to focus and be at peace, [repeating a mantra](https://liveanddare.com/mantra-meditation) is found easiest and most convenient by people of contemplative traditions. Such other ways as observing the breaths, [doing nothing]({{site.url}}/thinklist/nothing), [scanning the body]({{site.url}}/thinklist/scan), being present, gazing blankly at a spot, etc, may require a higher level of deliberateness that doesn’t come easy to many.

Yet, as easy as effortless as a mantra may be, it scares people - well, it scared me. It felt like madness to be repeating one thing or one sound over and over and over. And over. Worse is when it’s a babble from a foreign tongue. Please, don’t babble. Please, don’t speak in tongues. Please.

But repetition is the essence of mantra meditation. You let this one thing soak you up - yes, it is calming.

### Your mantra
If you’re keen on digging the practice of contemplative traditions, the Web is ready to take you in. There’s no end to that. But my advice is - simple and meaningful is your friend.

So, don’t go looking to the Buddhists or Christians for a mantra.

> “Don’t seek to follow in the footsteps of the wise. Seek what they sought. 
>
> ~ Basho

For those heavy on mantra, what they sought was mental peace. If they make wild claims to spiritual transcendence, still, know that peace of mind is enough for you. Again, simple.

For me, “thank you, O Jah. Thank you, O Jah. Thank you, O Jah. Thank you, O…” is enough. I come to mantra meditation like prayer. I know the meaning of those words. But I’ve repeated them so many times, I don’t think about the meaning any more (or I refuse to) - it’s become like meaningless sound waves. But the logic side of me is satisfied because it gets an answer whenever it questions “what’s that about?”

What’s that about anyways? I believe in a God whose name is Jehovah, the shortened form of which is Jah. And I don’t want to meditate on empty things. So, I reason that if I must mantra, it got to be meaningful - the angels must do this a lot, I guess. Enough.

But for you it may be different. [Some have suggested repeating “so hum. So. Hum. So. Hum.”](https://mindfulminutes.com/the-power-of-so-hum-mantra/) So on inhale, hum on exhale is the best mantra since “everybody breathes” and it should come natural. Really, hearing a typical so-hum meditation practice can be so sonorous and sweet, it’s calming music. But, you choose.

### Your session
You can set 22 minutes or shorter or longer. Take a few deep, cleansing breaths. Then start your mantra.

You may go fast or slow or at the pace of your breathing - in and out and in and out and…

You may say aloud or quietly or mentally (may depend where you’re having the session). Aloud may help you drown the mind’s ramblings better, the sound enjoyable too. But with practice, over time, even when silent (mental recitation), you can hear the sound in your head as you go over and over and o…

When your timer goes, gently release the mantra and come awake. Then let the few moments [after 22]({{site.url}}/after) work for you.