---
title: Find compatible
editors_notes: |-
    This think was originally titled "Check compatibility", but "Find compatible" had more emotive ring to it.
---
Asking “are we compatible” is definitely a loaded question - there is never an easy answer. But, it is also an introspective question - O yeah, accurately so.

You don’t directly ask a prospective spouse “are we compatible”? Anyone do that? But you ask yourself the question. Because the prospect never has the answer. Nor do you immediately have the answer. Really, you won’t have the answer at all. You’ll have an ordered scale against which to weigh the other person, question for question, after which, depending how much tolerance for deviations you allow, you can say “yes, we’re compatible, or no, we aren’t”. You’ll only come to the answer.

### The find compatible introspection
The answer comes from the introspection the question elicits. Now, it is that special introspection that makes it a worthy think for [22]({{site.url}}/what/).

The “find compatible” introspection is an honest inventory of you with respect to qualities you have, might have, and things you do, might do that a prospective partner may like or dislike, can put up with or can't stand.

When doing this introspection, you avoid the generic, common things or qualities about yourself and really courageously face the ones you believe stand out, make you unique, different, if not idiosyncratic. Really go into you and dig these out.

### Here’s an example
_The gender here is female, not because only males find (or should find) compatible, but because I chose to use myself - male man - as example. I would encourage my future female to do the same._

1. I can’t put up with people who don’t read. In fact, I’m completely put off when I hear “I don’t like to read”. 

    Does she like reading? Does she read?

    __I'd decide whether to ask this emanating questions directly or get answers with an open-ended question that helped me to draw a conclusion.__ Maybe not, “do you like to read?” But, “what’s your favorite book?” Or, “how do you manage to read despite your [work] schedule?” 
2. I believe no human should be fat. A person may have a wider or slimmer frame than another, but everyone should be trim and upright. I mean upright literally.

    Is she fat? Are there signs she could be fat?

    __If other answers may override this one, what steps is she taking to be and stay trim? Does she believe she can be trim?__
3. I can no longer stand cooking and eating meat, or, really any animal product or by-product. 

    Is she vegan? Or, can she put up with a vegan diet? Can she be vegan?
4. I believe calm is the secret to a rich full life and I want to build a brand around that. 

    Is she calm? Can she be calm? Can she build this brand with me? Does she believe you can’t be calm if you’re not upright morally, the other upright?
5. I’m very big on how clean the utensils are, how they’re arranged. The bathroom/toilet - is it spick-and-span? Do you squeeze the toothpaste from the end or the middle of the tube? You get the idea. 

    Does she care about things like that? Is she meticulous?

    __I’d look for an opportunity to visit where she lives/stays or bring her over to my place to see this for myself. Obviously, just asking the question may not give the true answer.__
6. I really hate clutter - I’m minimalist to my very core. Usually, one or a few things will do. I can be caught in only one pair of shoes for as long as it lasts - which can be 2 to 3 years.

    Is she the same? Can she make do with a few things, keeping good few things that last? Or, is she driven by fad, trend, and keeping up with peers? Does she get a high from shopping, acquiring? Does she draw comfort/confidence from things?

    __Visiting her and getting her to examine minimalist ideals, discussing the subject, are among ways I can determine what to do here.__
7. I can neither stand nor have the typical wedding - the one that is done to please the parents-in-law, that is merely a high-society wannabe, that costs so much one or both spouses have to borrow to have it, that leaves the newlyweds in crippling debt, that easily becomes 6 weddings (engagement, pre-introduction, introduction, traditional, white, court, you name it), that is more rooted in a fairy-tale idea of the "big day" than the reality of a lifetime of two unique humans wedded, that is done for the sake of Pinterest/Instagram, and that, of course, is exploited by [the wedding-idustrial complex](https://theweek.com/articles/463257/wedding-industrial-complex){:target="_blank" rel="noopener"}.

    Can we just marry if or since we really want to?
8. I love to take these long 1-2-hour walks, usually in the evening where I don’t go along with the phone or any technology - disconnected from everything else except the present moment of my walking and absorbing sights and sounds and whatever nature I find.

    Is she into that - nature? Walking? Running? Disconnecting? Reconnecting with the present?
9.  Is she attentive, present, to high-consciousness stuff? Spirituality? How does she view mindfulness, meditation? Does she believe in God - what’s her view - as a person or some force? How’s she connect to/with that personality?

### Keep updating your self knowledge
To be sure, the “find compatible” think doesn’t end with one 22. You can't fully inventory yourself once and so in 22 minutes. Nor should you do it once. I find it more revealing when I take it easy and come back to it a second and a third time and keep at it each time the need arises - incidentally, we can never know ourselves completely. We have to keep updating that knowledge.

As you date your future spouse, finding your answers, you’ll keep needing to come back to 22 to see how you decide. In my example above, she, too, has to be doing the same. Hopefully, both of you reach satisfactory answer - yes or no - and remain at peace with each other and the world.