---
title: Anticipate events
---
Anticipating events is to [deciding action]({{site.url}}/thinklist/decide/) what peanuts are to bananas, if you will. Either one is peanuts or bananas. While you can have the one perfectly okay without the other, the magic of delight starts to happen when you mix them. 

What I’ve found is that anticipating events ahead of deciding action helps me decide better actions. Of course, certain times I have my actions decided come what may.

### Let me differentiate
You thinking deciding action and anticipating events are the same? I wondered why not too. But the former is more about ethics and principles, as if you could declare “no matter what these things are, I won’t break my laws”. You got your laws. 

But, there are actions you could take that have nothing to do with your principles but are expedient and have to be taken. Such actions come easy if events are anticipated. So, how do you anticipate events?

### The technique & example
Say you got to give a conference talk. You prepare it, you know or determine what its purpose is or should be, you define a scope for it, you figure what your audience is like - some deciding action here. 

Now, based on what you’ve prepared, you anticipate reactions your audience may have, questions they may ask. Answer these questions or note where to point them for answers. 

That’s but one of a myriad examples of anticipating events. And it's something well worth the doing, your 22.

{% include components/break.html %}

I’ve found anticipating events makes me better prepared, helps me project better outcomes, because my understanding of things to come is boosted. You get more confidence, more charisma in delivery or in your actions at or for this event.

### It's practical
This practice helps you form such a vivid mental picture of things ahead of you that you’re able to focus on your objectives, your resolves. But it can also help you avoid trouble. You’re supposed to look forward to the good things, right? If then all the boxes of danger were ticked when visualizing all that might happen at an event, wouldn't you decline attendance and save yourself the trouble? Of course, you would. 

It matters that you have an idea how events may turn out.

### Some questions I pondered
**Q.** Does it matter that you know how events turn out?

**A.** Yes. Look at the example and its practicality again and see if doing this doesn’t make sense. 

**Q.** But a typical event already has an agenda of things to happen. Why bother extrapolating on these?

**A.** You still should in order to decide better actions, keep with your principles, get confidence for knowing your place at the event. To be sure is calming.

**Q.** Doesn’t this negate your [idk]({{ site.url }}/idk/) principle? 

**A.** Not exactly. Which is a call to caution. When you’re anticipating events, you still very much need to stay within your circle of competence. 

**Q.** What if you just don’t know or care what happens so as [to be natural]({{ site.url }}/be/) or let the natural course do its thing?

**A.** I think you can still know how to do or be based on how things may turn out and yet remain natural. But what do you think?