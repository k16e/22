---
title: Do nothing
---
You know when they encourage you [to go blank]({{ site.url }}/thoughts/)? To just sit or lie there and not think anything? To “empty your mind”? This is it. The “do nothing” technique for 22 is the quintessence of calm - if you get it right. 

When you run through the [22 thinks list]({{ site.url }}/thinklist/), you’ll see all of them involve some kind of action of the mind, except the do nothing. So, you want to intersperse your 22 sessions with it. I especially enjoy it every siesta; it’s my [power nap](https://en.wikipedia.org/wiki/Power_nap){:target="_blank" rel="noopener"} during the day and early evenings.

### The technique & example
Take whatever pose that leaves you comfortable and let your mind go, let it drift, let it butterfly from nectar to nectar. Goodness, what it likes to do! 

It’s ironic I call "do-nothing" a think, because you don’t “think”, not in the sense of actively doing it. You just let go. 

A flash of fun you had on a recent trip? Let it be, don’t try and understand it or control how long you dwell on it. Replay of your embarrassment over how a coworker addressed a goof you made earlier? Let it flow. Don’t try to make up for it, don’t wonder what you could’ve done or could do next time. Just experience it. Thoughts of sex coming in? Stay in them for as long as they last, but don’t be the one deliberating or prolonging them. Only experience here now where the mind has taken you. 

Remember, [it’s 22]({{ site.url }}/distractions/), so you can’t get up or move! Just saying.

{% include components/break.html %}

Truth is, this webpage will run endless if I were to list how many events or places a typical monkey mind jumps to and from in a minute - multiply that by 22. 

Just set your mind free and [free yourself]({{ site.url }}/be/). If there’s any control you have at all it's the one to refuse to control the flow of thought or what thought itself you should have. This is one ticket to rest - sleep and wake and sleep and wake but continue nothing for the duration you set - 22 or longer.