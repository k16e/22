---
title: Get in touch with you
---
Your body needs you to be aware of it.

If you wondered what for or why in the world will the body by itself need such awareness, know that it is for what _you_ can get from it. So, what do you gain from getting in touch with you?

### Why get in touch with you
What’s your body need or doesn’t need to thrive? Where do you feel a sensation and how does it feel? How does it feel to feel it - pleasant, neutral, or unpleasant?

* Is it a sensation of pain? How have you dealt with it?
* Is increased awareness of it and how you’re dealing with it not helping you to endure it better?
* Is it knots in your stomach? What emotions might have brought them? Fear? Anxiety? Pressure? Are these knots circling on?
* Is sensing them not helping you to know your emotions better?
* Does knowing your emotions better not help you easily learn [how to balance them]({{site.url}}/find-center/)?
* Could your mind use a [calming reverie in these sensations](https://mbsrtraining.com/body-scan-benefits/){:target="_blank" rel="noopener"} than the usual distracting thoughts?

How beneficial the mindful practice of getting in touch with yourself (or full body scan) depends how you answer those bullets. It is a [22 think]({{site.url}}/thinklist/) because it always benefits you. 

A [full body scan](https://www.mindful.org/the-body-scan-practice/){:target="_blank" rel="noopener"} is also known to train your attention - increasing your focus. It helps to release pent-up emotions. Ultimately, especially where you take the typical [22-savasana]({{site.url}}/savasana/) pose and sleep between scans, it is for the enjoyment of relaxation.  

### How to get in touch with you
On YouTube, self-help websites, and podcasts, there's an abundance of guided body-scan meditations. If you don’t know where to start at all, or prefer someone telling you what to do to meditate, go ahead, use the videos and audios.

To me, though, guided meditation is unnecessary. You only need to learn what to do, how to do it, and why to do it, then go do it. The why to do it is already clear from our talk so far, right?

The what to do is that you’re scanning your body from head to toes or toes to head or randomly as you feel sensations. 

The how to do it is where someone might think they need guidance. The best guidance you need is you - keep practicing zooming your attention in without distraction on an area of the body. When other thoughts get in the way, acknowledge them kindly and continue. As you do so from session to session, you’ll soon have a fine-grained control of where you place your focus and be more attentive. 

But you could still err, and have a longer, interrupted, or disturbed session. I’m not sure there’s any perfection in this. You just keep coming back to it, aiming to be better each session, knowing the why to do it.

You may sit in a quarter, half, full, or whatever-size [lotus position](https://en.wikipedia.org/wiki/Lotus_position){:target="_blank" rel="noopener"} leaves you [centered]({{site.url}}/find-center/) and comfortable on a mat or a firm mattress. You may also lie savasana on either of these surfaces. Whether you sit or lie, feel your muscles loosen, be relaxed, be still, then bring your attention to any part of your body where you feel the following sensations: 

1. Weight
2. Lightness
3. Tingling
4. Itchiness
5. Warmth
6. Chill
7. Looseness
8. Tightening
9. Ache
10. Nothing

From whatever part of your body you start, try to go in clockwise direction till you get back to that starting place. Because it’s easier to remember head to toes and vice versa, [many practitioners](https://zenhabits.net/practice-2-body-scan/){:target="_blank" rel="noopener"} tend to prefer that format. But like I’ve said, let it be what works for you.

As you bring attention to a sensation, feel it. What’s it feel like to have it? If it burns what’s it feel like to do so? Is it connected to any emotions or thoughts? Does it soften if the emotion softens and vice versa? You may have taken medication for an ache, but it’s still there. And now you’ve brought full awareness to it. How’s that feel? Helpful? More painful? Don’t judge...

Now move on to the next part of your body in the cycle. Keep on until you come full circle.

A typical body scan may last anywhere from 3-12 minutes, depending on how you feel at any set time. You’re allowed to snooze during the session - really, if you set 22 minutes for it, you can use the minutes left for a calming nap. Do enjoy getting in touch with you.