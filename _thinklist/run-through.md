---
title: Run through your day
---
Why should you run through the previous day? In the spirit of [letting yourself be]({{site.url}}/be/) and being present today, right here where you are, yesterday is gone, why bother about it?

Wait, can you just try an experiment? Try running through the day before yesterday, or even the day before this one - accounting for from when you woke up to when you went to sleep. Can you even start? As if your head would hurt. The brain’s resisting. It’s impossible, right? 

Sure, certain events may still be fresh in mind and may trigger your remembering what came next or before and so on. But the brain seems already done with that day. The brain has committed to its long-term store those key events that your consciousness assure it will be needed in future, then cleared its working memory, from where you could’ve done your quick run-through, to make room for the closest events.

So, technically, you have only yesterday (or the most recent time) to run through, to retrieve events from. Yet, is it necessary to do so?

I still struggle with that. But I agree that there’s two reasons why I should be doing this in order that I might find areas where correction is needed, where I need help, where I could improve.

### Two reasons to run through yesterday
The two reasons to run through yesterday will answer the questions:

1. __Did I heal somebody?__ How or by what action or actions? How can I do more or better?
2. __Did I hurt somebody?__ How or by what action or actions? How can I forestall further hurt? How can I not hurt in a similar fashion or in any manner at all?

These two questions as well as the added benefit of [exercising your memory]({{site.url}}/memory/) thus building it are good reasons why you should add this think to your list, using some mornings’ [22]({{site.url}}/what/) so. Incidentally, the morning, I’ve found, is the most suitable time for this session.

### How do you run through your day?
In order to know when any of those two questions happened, you need to “inventory stops” in the day you’re trying to retrieve. Stops can be, yeah, stops in your daily routine. 

For example, what happened when you woke up? What did you do first? What’d you think about? Where did you go? What happened in the bathroom? At breakfast? When you stepped out the first time that morning? Into your car? On the road? On the bus? Soon as you got to the workplace? Lunch time? When you operated that equipment? At that meeting? In traffic? And so on until you got back home and retired to bed.

Phew.

It does seem tedious and unnecessary because, like I said at the outset, you could just be and let yesterday go like it’s gone.

### Eliminating the tedium
Try the following activities to cut the tedium down or out of this “run through your day” think:

1. When you notice yourself getting bored and tired, let yourself sleep off or drift into [“do nothing”]({{site.url}}/thinklist/nothing/), not interrupting your mind at all. This practice is for [calm]({{site.url}}/calm/), not agitation - don’t struggle with it.
2. Drop continuance if you can determine that yesterday held nothing “monumental”, well, at least to you. That said, you want to limit this think to days when things stick out to you.
3. Don’t force your retrieval into some linearity of occurrences. Don’t trouble if you feel you’re missing a few happenings. Like in 2 above, let the things that stick be the ones that stick.
4. Don’t struggle to inventory too deep. Still, don’t be too cursory. But let it be like in 2 and 3 above.

### Your brain is lazy
The brain knows the hard work of recollection and communicates that to your consciousness. It's too hard! It tries to scare you. Don't even start! It tries to get you to not do it. Do it anyway. 

There’s no or very little pattern to this, and the brain loves pattern, because it’s inherently lazy, and likes to be on autopilot, to conserve it’s energy… do it in spite of the brain. Do it anyway.