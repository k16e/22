---
title: Before you buy
---
You can have a 22 session before making a decision for or against buying or introducing a new thing into your [space]({{site.url}}/words/space/).

Some of the principles that help in [deciding action]({{site.url}}/thinklist/decide/) are applicable here. But this “before you buy” think is very specific to buying decisions. 

>   A calm person should not suffer buyer’s remorse.

### On buyer’s remorse
Our main concern here to avoid [buyer’s remorse](https://en.m.wikipedia.org/wiki/Buyer%27s_remorse){:target="_blank" rel="noopener"} and long-term self-hurt, wastage, or needless accumulation is much less about “resources invested” and more about ‘compatibility with goals’.

I’m proposing 7 questions we must sincerely answer, based on which we may go ahead with a purchase or humbly and happily decline.

### Before you buy ask
1. What’s the purpose of this thing?
2. Is this purpose lofty to you and others? Lofty in terms of character and spirit. Is it a good thing? Is it [happy]({{site.url}}/happy/)?
3. Lofty, yeah, but may not fit _your own_ purpose in life. So, does it fit your overarching purpose in life?
4. Do you have a place for this thing? Is the timing right for you to get it? This won’t be an easy “yes” or “no”; you’ll do a bit of work thinking through why the time may be right or not.
5. If question-groups 1 through 4 are satisfied, can you then list like 7 ways this thing can benefit you - what can you do with it? Can you live with it happy?
6. Negate your desire a little bit - what can you do without it? Can you live without it?
7. Up the ante on the negation just a tad - what if you can’t have it, say you can’t afford this thing, what do you do?

I hope you see that these are serious questions, which is why only a session like [22]({{site.url}}/what/) can help toward getting satisfactory answers.

### You’ll be the happier after
Taking out 22 to do this, perhaps a couple 22’s for big purchase decisions, you’ll have sufficient time to calm the heightened “anticipation of the enjoyment that [could] accompany using [even just owning] the [thing].” Thus, you’ll be empowered with an informed decision, responding instead of merely reacting.

In a life of calm and minimalism, it’s a joy to look around you each time and be pleased with your enough.