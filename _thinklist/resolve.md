---
title: Resolve something
---
First of all, pick one topic, one subject, or [one thought]({{ site.url }}/thoughts/) and mull it over, if all 22 minutes is enough time, or note where to continue during your next session. Be sure to get some clarification. This you'll document [after 22]({{ site.url }}/after/). 

In the early days of 22, "resolving something" was almost the order of the sessions for me. By "resolving 22" I was able to [define the practice]({{ site.url }}/what), [how to do it]({{ site.url }}/how/), its [benefits]({{ site.url }}/why/), and how I'd run the program. 

Having grown in the exercise, I've had to try other thought processes on this list. But resolving something was the mother ship.

### The technique
1.  What is this thing you want to resolve? Try to define it, however crude. Try to get a bird's eye view first. Then,
2.  Ask whys and what-ifs and hows. Ask these in series of each other, as if questioning different aspects of your subject matter, questioning different parts thereof,
3.  Try to see an outline emanate of discussion points about this subject. You'd document this outline [after a 22 session]({{ site.url }}/after/) and build on it until the matter is resolved,
4.  Then ask whys and what-ifs and hows again, in case you must return to it.
5.  Welcome [sleep and do sleep]({{ site.url }}/sleep/) when it bores you.

### An example
"What is 22, kb?" "I mean, to you, how’d you define this thing?" Then I defined it. Before [the well thought-out definition]({{ site.url }}/what/), 22 was just “a mini MRI daily”, “22 minutes of negative space in your life everyday”, “22 minutes of empty space daily”, “22mins of silence”, “22 minutes of stillness”, until I drilled it down to a tagline—“Find you in 22”, “Find. You.” 

As you can see, taking several crude shots at defining it helped bring me closer to a resolution. With a series of whys and what-ifs and hows I've resolved 22. I've founded a model. 

“Why 22 minutes, what if longer?” “Or shorter?” “Why the 22 number?” “Or another, bigger or smaller?” “What if nobody else does 22 but me?” "Or everyone thinks what a crappy idea you got?" Incidentally, I'll never stop even if I’m the only one doing 22!

### Probing further
“How much do you know yet?” “Why not start a blog and share what you know?" “Why a blog, what if something else?” “What if just a newsletter?” “Or a Facebook page?" “How’d this 22 evolve?” “What might you become?” “Where might this take you?” “What if you become very famous?” “Aren't you just trying to become famous?” “How'd you handle fame?” 

“Is this where you want to focus in life?” “Wouldn't you want to make money with 22?” “What might the business model be?” “Can you justify earning money from it?” “Is it even worth it?” "Are you going to be a guru?" "Really?" 

To be sure, there was much more probing through several sessions, all of which, thankfully, has brought me this far. 

Actually, I'm still probing as much as I believe I've resolved it. Again and again, I find myself going over these what's and how's and why's. And, just as when I started, I continue to sleep off through several sessions, only to be woken up by the alarm at the end and sometimes way past the end.

{% include components/break.html %}

Don’t think any light of this “resolving something” think process. It can be very intense, last through several 22’s, and definitely birth something of surprising magnitude. Even while boring you to sleep! Resolution, like sleep, is happifying, calming.