---
title: A tweak to my 22 lifestyle
date: 2019-02-10
themes:
    - sleep better
description: |-
    The recipe for learning anything is made of four ingredients. What're they? How can you use them?
---
You know that 22 is the literal practice of sitting or lying still for 22 minutes more or less? The discipline of calm is learned from this deliberate activity (or lack thereof) of stillness. Because, more importantly, 22 is a lifestyle of living calmly, of consciously noticing every facet of a self-culture as filtered through calm, being even-keeled.

So right there you got 22 as two - a physical practice and a calm culture. For reference, let's label the one "pp" and the other "cc". Cool? I've tweaked how I do the pp majorly. I mean, if I've reduced my number of sessions from 3 to 1, that's major, right? My recommendation has been to find time to do 22 up to 3 times daily, and I've done so for years (actually, 1 and a half).

However, I've reasoned that 22, which I've forever touted as panacea for sleep, may have gotten in the way of my sleep. Surprised?

### Sorry, I've run out of melatonin
When I've woken up 2:50am some nights, 12:50 some, and 1:50 and and not able to get back to sleep again till the next night, it's as if my brain responded to my prodding, "sorry, I've run out of melatonin, I can no longer do this." By the way, the 50 number isn't made up - can anyone explain the coincidence? It beats me.

It's as if the calm from my early evening nap of 22 postpones production of melatonin so that, come 9:25pm when I'm accustomed to go to sleep, I'm still active. Then lying in bed is like forcing sleep to come - some nights it comes, some nights it doesn't. Even when it does come, as soon as 11pm or midnight, I wake up done.