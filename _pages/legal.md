---
title: Legal Notices
layout: legal
permalink: /legal/
position: 3
last_updated_at: 2018-11-12
description: Disclaimer to 22, terms of use of website, privacy policy, and credits to assets and content on 22.
disclaimer:
    title: Disclaimer
    body: |-
        ### Disclaimer
        I only recommend 22. I don't prescribe it. I don't preach it either&mdash;it's not religion!
        
        While I believe in its power to becalm the practitioner and liver by its culture, as much as it has helped me, I do so without any guarantees. I'd appreciate a community where every self-actualized member can make their own wholehearted decisions as to what they practice.

        Again, 22 is an exercise, as in calisthenics or aerobics, not a prescriptive medicine nor a religion, nor a cult.
copyright: 
    title: Copyright Policy
    body: |-
        ### Copyright Policy
        Just like with one of my influencers, Leo Babauta of [Zen Habits](https://zenhabits.net/uncopyright/){:target="_blank" rel="noopener"}, 22 by Kabolobari is completely copyright-free. 

        Every piece of content I ever publish here is in the public domain.
privacy: 
    title: Privacy Policy
    body: |-
        ### Privacy Policy
        I respect your privacy even more than I expect every setup to respect mine. 
        
        That said, I will *never* share, rent, sell any personal information from you. That is, if any interaction with me on 22 warrants my collecting such information.

        Furthermore, I steer clear of practices that may jeopardize your privacy. Such practices as spam, interruptive advertising, needless personalization of public content, and tracking are not in my management plan.

        I may use web analytics only to see how people (anonymized) are using this site but that's the very limit I do go with such technology.
credits: 
    title: Credits
    body: |-
        ### Credits
        I love to give credits to my erstwhile girlfriend Amina Ibrahim. How could I have started this journey without you. Hope you keep at 22 as long as life permits.

        I continue also to give credits to Merriam-Webster, Incorporated both for the [Learner's Dictionary](http://www.learnersdictionary.com/){:target="_blank" rel="noopener"} and [Merriam-Webster](https://www.merriam-webster.com/){:target="_blank" rel="noopener"} online references - every definition in [Words]({{site.url}}/words/) has come from either or both of these two great resources.
about_owner:
    body: |-
        ### 22 is by Kabolobari Benakole
        I am Kabolobari Benakole and all references to "I" on this page and all others on this website are to me. 
        
        I own and manage this website. I also style myself as "curator of the culture of calm", meaning that I live each day as steward for content that helps the community thrive in calm.
---
