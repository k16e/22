---
title: About 22
layout: about
permalink: /about/
position: 2
description: If a "calm heart [can give] life to the body" I believe a culture of calm can enliven our world.
author_email: 'kb@22.center'
about_22:
    title: An exercise of calm
    body: |-
        ### An exercise of calm
        22 is an exercise of [calm]({{ site.url }}/calm), to put it simply. Because that's what it produces. It lets you tap in the very many benefits of calm, such as, enlivenment, sound actions, discernment, and destressing.
        
        For me, 22 is a constant reminder that I got to be calm no matter what. 
        
        That mentality plus the physical benefits of the shavasana-like exercise itself is what this is all about. Plus, I'm looking to build a caring community about this culture where everyone can breathe easy with nothing to fret about.

        My only admonition? Start doing 22. Be. Calm.
about_kb:
    title: About Kabolobari
    body: |-
        ### About Kabolobari
        ![Bust of Kabolobari]({{ site.url }}/uploads/avatars/kb.jpg) Hi, I'm Kabolobari Benakole or kb.
        
        Calm's a journey I embarked on after my most fortunate medical experience ever - the MRI. I’d love to invite you on this journey, especially for its benefits in crucial virtues.

        A bit of me from my [mri](/mri) experience, I'd give a vehement no if anyone ever thought I was or declared me calm. Today, it's a path I cannot veer off of. 

        Of course, modern medicine works but I also eat only plants and believe veganism is medicine. Yet overall, calm, and this deliberate practice of it with 22 is my first call for any ail. As if I say, "keep calm first and the rest will follow." 

        So, my playbook is straight out of Proverbs 14:30 - loosely quoted at the outset. 
        
        Calm is my healing.
footnotes:
    body: |-
        ### Credits
        **Bust of Kabolobari** taken by
---
