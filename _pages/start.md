---
title: Start here
layout: start
permalink: /start/
position: 1
description: Learn what 22 is, how to start and persist in the practice and culture, and enjoy emotional health and calm.
what:
    title: What is 22?
    body: |-
        ### What is 22?
        22 is an exercise of [calm]({{site.url}}/calm/), where, like in [savasana (yoga)]({{site.url}}/savasana/), you let everything go for 22 minutes. Yes, til your alarm goes, "come back to the world" with a buzz - buzz - buzz. [Full definition and explanation here.]({{site.url}}/what/)
why: 
    title: Why do 22?
    body: |-
        ### Why do 22?
        The calm I got in my head and overall after the [MRI]({{site.url}}/mri/) was so good I couldn't help but want it all the time. Now, after I tried it a couple times and landed on what seems to be the best duration for a typical exercise of this type, 22 minutes, I've since got very [many benefits]({{site.url}}/why/).
how: 
    title: How do you do 22?
    body: |-
        ### How do you do 22?
        All you need to do 22 is a mind that is prepared to stay calm and not trouble or wander for as long as 22 minutes or longer or shorter depending on your preference. [More how.]({{site.url}}/how/)
list:
    title: Your thinklist
    body: |-
        ### Your thinklist for 22
        Here's [a growing collection of "thinks"]({{site.url}}/thinklist/) you can have during 22. My advice is that you limit mental activity to one think only each session or nothing in order to get that total makeover from the practice.
---