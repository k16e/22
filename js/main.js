// Main JS File

    // @package  22
    // @author   Kabolobari Benakole <kb@22.center>

// Selector shortening helpers
function q(selector, parent) { return (parent ? parent : document).querySelector(selector); };
function qq(selector, parent) { return Array.from((parent ? parent : document).querySelectorAll(selector)); };

// Highlight the essays link in mainnav when on a single essays page
(function() {
    'use strict';

    const
        singlePage      = q('.is-single'),
        aWordsPage      = document.location.pathname.indexOf('words') == -1,
        aThinklistPage  = document.location.pathname.indexOf('thinklist') == -1;

    if (singlePage && aWordsPage && aThinklistPage) navHighlighting();

    function navHighlighting() {
        let nav = qq('[nav-list] li a');

        nav.forEach(el => {
            if (el.href.includes('essays')) el.setAttribute('class', 'is-active');
        });
    }
}());

// Select the first character and do stuff with it
(function() {
    'use strict';

    const getsDropcap = q('.post-content.gets-dropcap > p:first-of-type');

    if (getsDropcap) markUpFirstChar(getsDropcap);

    function markUpFirstChar(el) {
        let span = document.createElement('span');

        span.className      = 'first-character';
        span.textContent    = el.textContent.charAt(0);
        el.innerHTML        = el.innerHTML.slice(1);

        el.insertBefore(span, el.firstChild);
    }
}());


// Replace all occurrences of ' - ' with em dash
(function () {
    'use strict';

    const content = q('[content-start]');

    if (!content) return;
    content.innerHTML = content.innerHTML.replace(/ - /g, '&mdash;');
}());


// Wrapping a responsive div setting around table
(function() {
    'use strict';

    const
        tables = qq('.post-content table');

    tables.forEach(el => {
        let
            previousSibling = el.previousElementSibling,
            newDiv          = document.createElement('div');

        newDiv.setAttribute('class', 'table-responsive');
        el.remove();
        previousSibling.insertAdjacentElement('afterend', newDiv);
        newDiv.insertAdjacentElement('afterbegin', el);
    });
}());


/**
 * Animated Accordion
 *
 * @author      Kabolobari Benakole <kb@22.center>
 * @see         {@link https://gomakethings.com/how-to-add-transition-animations-to-vanilla-javascript-show-and-hide-methods/}
 * @copyright   2019 22 Center
*/
(function() {
    'use strict';

    document.addEventListener('click', (e) => {
        if (!e.target.hasAttribute('accordion-toggler')) return;
        e.preventDefault();

        let
            el      = document.querySelector(e.target.hash),
            parent  = e.target.parentNode;

        toggleAccordion(el);
        toggleClass(parent);
    }, false);

    function toggleAccordion(el) {
        if (el.classList.contains('is-expanded')) {
            hideAccordion(el);
        } else {
            expandAccordion(el);
        }
    }

    function expandAccordion(el) {
        let
            height      = getHeight(el),
            activeAccds = qq('.accordion-content.is-expanded');

        activeAccds.forEach(el => {
            hideAccordion(el);
            toggleClass(el.parentNode);
        });
        el.classList.add('is-expanded');
        el.style.height = height;
        el.addEventListener('transitionend', () => {
            el.style.height = '';
        });
    }

    function hideAccordion(el) { // set height to a fixed number, then wait a split-second to animate from there to 0
        el.style.height = getHeight(el);
        window.setTimeout( () => {
            el.style.height = '0';
        }, 1);
        el.classList.remove('is-expanded');
    }

    function getHeight(el) { // get the height of the element, el, passed in
        let height = el.scrollHeight + 'px';

        return height;
    }

    function toggleClass(el) { // add/remove .is-active class on parent .accordion-item itself
        if (el.classList.contains('is-active')) {
            el.classList.remove('is-active');
        } else {
            el.classList.add('is-active');
        }
    }
}());


/**
 * Show/Hide Header + Footer
 *
 * @author      Kabolobari Benakole <kb@22.center>
 * @copyright   2019 22 Center
*/
if (!q('.is-home')) {
    (function() {
        'use strict';

        let
            header      = q('[site-header]'),
            footer      = q('[site-footer]'),
            prevYPos    = 0;

        window.addEventListener('scroll', function() {
            let currYPos = window.pageYOffset;

            if (currYPos < prevYPos || currYPos === 0 || (window.innerHeight + currYPos) >= document.body.offsetHeight) {
                header.classList.add('in-view');
                footer.classList.add('in-view');
            } else {
                header.classList.remove('in-view');
                footer.classList.remove('in-view');
            }

            prevYPos = currYPos <= 0 ? 0 : currYPos;
        }, false);
    }());
}


// Set such attributes as stated to all external links
(function() {
    'use strict';

    let allA = qq('[content-start] a[href^=http]');

    allA.forEach(e => {
        if (e.hostname !== location.hostname) {
            e.setAttribute('target', '_blank');
            e.setAttribute('rel', 'noopener');
        }
    });
}());


// Format blockquotes semantically
if (q('blockquote')) {
    (function() {
        'use strict';

        if (!q('blockquote p:nth-child(2)')) return;

        let citePara = qq('blockquote p:nth-child(2)');

        citePara.forEach(para => {
            let
                citeContent = para.textContent,
                cite        = document.createElement('cite');

            cite.textContent = citeContent;
            para.parentNode.replaceChild(cite, para);
        });
    }());
}

// Page transition animations
(function() {
    'use strict';

    var
        allA = qq('a'),
        locA = allA.filter(a => a.hostname === location.hostname && a.hash === ''),
        main = q('main');

    locA.forEach(a => a.addEventListener('click', e => a.pathname === location.pathname ? e.preventDefault() : main.classList.add('fade-out-down'), false));
}());


// Floating label
(function () {
    'use strict'

    var inputs = qq('.form-control input:not([type="submit"]), .form-control textarea');

    inputs.forEach(input => {
        var
            parent = input.parentNode,
            label  = q('label', parent),
            span   = document.createElement('span');

        span.className = 'required';
        span.textContent = `*`;

        if (input.hasAttribute('data-required')) input.nextElementSibling.append(span);

        input.addEventListener('focus', () => label.classList.add('active'), false);

        input.addEventListener('blur', () => {
            if (input.value.length < 1) {
                label.classList.remove('active');
            }
        }, false);

        if (input.value.length > 0) {
            label.classList.add('active');
        }
    });
})();


// Character Count for Textareas
if (q('textarea')) {
    (function() {
        'use strict';

        var els   = qq('textarea');

        els.forEach((el) => el.addEventListener('input', () => q('.counter').textContent = Number(el.value.length), false));
    }());
}


// Focus textarea as soon as faq li that has it is expanded
if (q('.is-faqs')) {
    (function() {
        'use strict';

        q('.accordion-item:last-of-type').addEventListener('click', () => q('textarea').focus(), false);
    }());
}