---
title: When urge to complain comes
date: 2018-09-09
themes:
    - stay centered
    - have gratitude
description: |-
    What do you do when urge to complain comes? Should you have no choice but complain? Can you stop?
footnotes: |-
    ### Noteworthy reads
    - [How Complaining Rewires Your Brain For Negativity](https://www.huffingtonpost.com/dr-travis-bradberry/how-complaining-rewires-y_b_13634470.html){:target="_blank" rel="noopener"}
    - [10 Ways to Complain Less (and Be Happier)](https://tinybuddha.com/blog/10-ways-to-complain-less-and-be-happier/){:target="_blank" rel="noopener"}
---
“O, no, I hate this!” “This is wrong.” “Why they do things like this?” “Aren’t they thinking?” “Why do you make such boneheaded decisions?” “These folks can never get it right!” “I am done with your lousy restaurant!” 

Whether you’re muttering or murmuring such expressions to yourself or ranting them out to a close circle or even on Twitter, two very bad things are happening whenever you’re complaining, none of which is solution to the “lousy service” or whatever the abnormalness was.

Now because we're designed with a body - and its thousand needs and wants - which we must satisfy on first call, our brain is quick to alert us when something deviates from the “normals“ or "satisfyings” of any one of these needs and wants. We see the faults first, more often than not.

Again, because we’re each unique, we see these “abnormalnesses” a lot. 

What's going on? What can we do?

### When you complain
There’s _really three_ bad things that happen when we complain: 

1. You make a monster of what the abnormalness is or was because you draw inordinate amount of attention to it, and
2. You draw too much attention to your holier-than-thou self, because when you’re ranting, you’re saying, “I and people like me are different, such [insert whatever you’re ranting about] is so below me/us, people like me/us can’t be caught doing that.”

    How egotistic, and fallacious for that matter! Oops, I just complained, but you get the idea. Incidentally, we could call something out for what it is without necessarily vent, rant, or complain.

The third thing is kind of hidden so we do not always recognize it’s happening when we’re complaining. We’re hurting ourselves. 

When you complain, you have unconsciously given up on any solution that could be available and just submitted to worrying silently or loudly. “Well, let me just rant and get on with that, after all, will anything change?” Besides, the more you rant, the more traffic you get to yourself! So? But you’re getting chewed up inside by the perceived abnormalness, by stress, you don’t even know it and by how much...

Complaining depletes our [calm]({{site.url}}/words/calm/), overdraws on our well-being, and, sadly enough, tricks us with a false pleasure of relief that brings us back to itself - complaining. It’s why people who complain a lot, well, complain a lot and gravitate toward people who complain a lot - it’s a treadmill of, unfortunately, sweet negativity. The complaining loop or the complaining treadmill - it’s endless.

Now since things to complain about abound, our being unique, what should we do instead? How can we never complain again?

### When urge to complain comes
Mastering not-complaining is a skill. It’s a skill you learn by accepting that you can either do something about [insert whatever you could complain about here] or let it go and never speak or worry ill of it at all. Yes, you can either beat them or leave them. Alone.

You got to accept that. 

#### 1. Do you have a solution?
You feel resentment about how the road was recently redesigned. Ask, “is there something I can do about this?” Do you have a solution? Can you beat them? Can you spend what might rather be “complaining minutes” thinking through possible solutions? Can you report or publish your thoughts to the appropriate authority? And, when you do, not to rant at their “boneheaded decision”, but to help them see what difficulties arise because of their “design” and how they could do otherwise. Then, you got to wait patiently for your solution to be effected. Still, if not, to then let it all go - because you shouldn’t complain - remember the three bad things?

You’re either to put up with a perceived aberration or move on to something else you find "perfect" - find a new road, maybe.

#### 2. Show insight
Is there something I can do about this? Or show insight - “what if I don’t know enough to see why she chose to act or appear that way?” Because while you’re fixated on a seeming abnormality, you’ll have too little time to see past it or into it.

“You can never see through the door to the inside,” she said, “that’s how it was designed. So, if the door should say “PUSH” on the outside you might slam it on some one going out. So, “PULL”, it says. But someone going out, since they can see through to the outside, would take caution not to push if someone were outside trying to come in. So, “PUSH”.” Because I’d complained that I hated how their entrance wasn’t “welcoming enough” because it made one exercise too much effort pulling the door, that it’s easier to push in instead when coming in. Only until she explained did I see that the entrance door to the office was blinded outside-in. That was their purposeful design and it shouldn’t have been worth my complaining about. They know better. I took the lesson.

Imagine how many times I must have complained about poor door designs, I know…what if they got a reason for it? Or might there be something I could do? Show insight.

#### 3. Practice gratitude
One last thing you can do whenever the urge to complain comes, is remember [gratitude]({{site.url}}/words/gratitude/) - call your gratitude on whatever little you can see available amid the abnormalnesses. Because there’s always something to be grateful for.

Once you find something to be grateful for, make that do. Make it twice as much as what you’re eager to complain about or more.

{% include components/break.html %}

Make it a goal to never complain again. And because you’re a unique person, the urge to complain will always come. When it does, (1) can I do something about this, (2) might they know better, and (3) I got to see the sunrise this morning and it was beautiful.