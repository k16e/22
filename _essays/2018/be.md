---
title: Let yourself be
date: 2018-05-09
themes:
    - stresscare
    - sleep better
description: What's it mean to be? Because if you knew why you should be, it’d be easier to take such advice. Here's why.
updated_date: 2019-07-21
last_modified_author: Kabolobari Benakole
---
Be is a common admonition. Some of its renditions are *“be present”, “be intentional”, “be aware”, “be conscious”, “be deliberate”, “be mindful”…* All of them as beautiful as are rendered.

If you knew why you should be, it’d be easier to take such advice. Besides, you’d be more inclined to follow the how to be, the practice of being.

So, why should you be? And, how can you be?

### Why be?
If you’re reading this, hearing it, consuming it in some form, you know why you ought to be - you are living. By that I mean that living is the very reason you should be.

I know that’s as simplistic as it is confusing. Doesn't it beg the question - what’s it mean to be? Even then, I haven’t said a thing except “because you’re living.”

### To be
To understand what it means to be, I'll paint a simple picture what is happening when we’re not being, when we’re in a moment but not present in it.

You’re chatting someone physically, but checking WhatsApp. While answering incoming chats on WhatsApp, you’re also checking Instagram, just so you’re able to keep up. And you’re sure it’s possible to keep up with all these activities all at once. You tell yourself, “yeah, I can multitask, I’m good at that.”

You visit or spot a scenic sight and immediately reach out to your smartphone to, not only shoot to preserve the sight, but share with friends and family on social media. It’s interesting that you try to have the next thing even before you have gotten a touch of the first, your very being here. You give yourself not even a moment to take in the sight.

*As a side note, it’d be different for a “photographer” whose purpose for the scene was to shoot it for you. And you could go there as a photographer, but that’s another story.*

Compare those two examples with the vast amount of similar ways you’re not being when you should and you’ll begin to see what it means to be.

To be, you need to accept that [every moment has a unique purpose](/words/teleology/), even as all purposes - if your life is edited, planned, and deliberate - all roll into your cause or causes for living.

Thus, you need to accept each moment as it comes.

Try to understand it, not necessarily scientifically, not way too studied, lest you get caught up in knowing and miss experiencing, missing the moment.

Don’t grasp the scene and miss the moment. Don’t get the future but miss the present - really where else can you live?

I’ll permit you, just take a pause from taking this bit of information in. Think “what am I doing now”, what am I seeing, hearing, thinking,…? Be honest with yourself. Don’t judge your answers, don’t imagine what could come next, just be contented with what is now. Just be.

Now, you’re free to continue and endeavor to just be here and nowhere else. Because that’s the whole point of being - where you are, what you’re doing at the time of it, be there, be with it. You got a thousand and one other times to scratch an itch or fear a snake bite or worry your car would crash. But for now, you. Just. Got. To. Be.

### The do-not’s in being
In order to be, there’s about 10 “do not’s” you should experiment with, get comfortable with, and just be with. Put together, they’re a practice no other is worthier than.

While you could do any of these “not’s” at other times, you aren’t permitted to whenever you must be.

1. Do not want.
2. Do not need.
3. Do not desire.
4. Do not hope.
5. Do not wish.
6. Do not scratch.
7. Do not think.
8. Do not worry.
9. Do not fear.
10. Do nothing.

Terrifying? Well… I assure you with constant practice, it’s doable and, of course, enjoyable.

You may be familiar with “focus on your breath - its ins and outs”, a common admonition from meditation teachers. Another, like it, is “[study where your body or a part of your body makes contact with the ground, the floor, bed, or whatever it’s in contact with]({{site.url}}/thinklist/scan/)”.

You may also idly count a string of beads, like some religions prescribe for prayer. Or, follow a tick-tock-tick-tock of a clock somewhere; or the crackling of a fire; the patter of rain on the roof or just rain itself falling. Or [do your mantra](/thinklist/mantra). Any repetitive sound or rhythm around you would do.

You do these boring, repetitive things in order to remain in a be state and not do any of the “do not’s”.

Of course, you’d be excellent if you could be without any of these, to just submit to be. To just [yield](/patience). But I admit, be-ing may be impossible without.

Do not [distract](/distractions) your be for it is quite beneficial to you. As I explore this practice more, I'll in subsequent essays dish out practical reasons to be. Until more comes, you be.