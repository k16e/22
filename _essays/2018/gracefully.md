---
title: Postpone expectations gracefully
date: 2018-12-02
themes:
    - stresscare
    - lifestyle medicine
description: |-
    Since our desires are unique, our expectations will almost always be delayed or denied. How do we deal gracefully despite? 
cover_image:
editors_notes: |-
    Aabe is not the real name, but she'd appreciate if her pain is understood and someone has a better idea how to treat it.
footnotes: |-
    ### Materials Reviewed
    - [What I’ve Learned from 2 Years of Intermittent Fasting](https://jamesclear.com/good-bad-intermittent-fasting){:target="_blank" rel="noopener"}
---
Aabe can’t wait for the fog to clear.

The 38-year-old said her head feels as if it is held under a beret that is pulled down by a vise continually tightening in opposition to an expanding skull. 

“It’s very painful some days,” she said. “It degrades my quality of life. There’s mornings when I wake to severe heaviness and pain, as if the fog is still so dense and will only gradually lighten as the day warms, then I get some relief. I pain but I hope.”

Aabe has put up with this pain for many years, but she believes it will clear, someday. She doesn’t know when. She longs for it. She has carried it in the head since she was 22. 15 years later she sometimes fears she would live the rest of her life with it.

Having gone through several doctors, including specialist psychiatrists and neurosurgeons, brain scans and MRI’s, supple Aabe has turned to a regimen of diet, exercise, and mindfulness as the only way to get her head to clear. This regimen is how she’s coping.

### The suffering of expectations
When what we desire and expect fails to come, or delays, like a free head for Aabe, we may get disappointed, discontented, furious, even sad and depressed. Some take their own life.

To continue [centered]({{site.url}}/find-center/) and calm we need to [postpone our expectations]({{site.url}}/expectation/) without trouble, without struggle, without a bout of depression. We need to accept that what we expect may not come even on a later date. We need to postpone our expectations gracefully. 

What sort of attitude should we have or how should we be disposed when we must do so? Because as long as we continue with unique desires and efforts and circumstances, our expectations will almost always be varied, delayed, or denied.

### Postponing expectations gracefully
Let’s hold on to the following three guidelines in order to deal with delayed or denied expectations:

1. __Life may not depend on it.__ When we want or need something, our mind tends to exaggerate how good life would be with it, we would swoon to death should we miss it. If we could accept first that life would go on just fine without it, we would do well. 

    We cannot do without food and water, for example. But people coped when they were deprived of these. While such deprivations are less than ideal, and can kill us, our expectations are hardly ever on that scale. 

    Pretend that the very fact of living is enough.

2. __What blessing is being masked?__ What if the delay was only a blessing in disguise? Risking sounding callous, Aabe’s coping regimen may be the help the rest of the world needs, and she’s sacrificed a “normal” head to bless the many. What if she accepts that her condition helps her to help the many?

    We need to know that sometimes God allows these things to prevail in a way that unbeknown to us is masking better options. All we need is [grace]({{site.url}}/words/grace/) while waiting. Look for the blessings.

3. __What could you learn?__ Might you need endurance in future and this is your one opportunity to learn it? Strength training? You never know! It took some people being broke to learn that it was better for health and wellbeing to fast 10-14 hours every day. Now [doctors encourage fasting that long](https://fitness.mercola.com/sites/fitness/archive/2013/01/18/intermittent-fasting-approach.aspx#!){:target="_blank" rel="noopener"}.

    So what could you learn from this delay? What is it trying to teach you? Might it be willpower? Firmness? Simplicity? Well, wait and see - I mean, wait. Gracefully, of course.

To be graceful, you’re not going to worry or complain; not to say you’ll live the rest of your waiting in silent dejection. No, but you’re going be upbeat, easy, and [carefree]({{site.url}}/words/carefree/).

You may device a coping regimen like Aabe, but constantly hold these three lines in mind - life may not depend on it, might there a blessing? and what could I learn? When you do, you’ll see how much [grace]({{site.url}}/words/grace/) you’ll have while waiting and your peace will be abundant.