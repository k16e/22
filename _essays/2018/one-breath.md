---
title: One breath - a meditation
date: 2018-08-05
description: |-
    Take each breath in, as long as it can be called breath, with gratitude.
themes:
    - have gratitude
---
You can only have one breath. That is, you can’t have one breath twice - the same breath. That is, each breath is unique and comes in once, goes out, and is gone.

Think about that for a moment.

{% include components/break.html %}

Now breath is nothing. It’s nothing before you and nothing after you. It’s something only because you can breathe. It’s something only once - as soon as you breathe in. Out and it reverts to nothing. From it when in to nothing. It. Breath. Nothing.

Think about that, too.

{% include components/break.html %}

How many unique breaths do you need to stay alive? Nobody knows. You can have breath multiplied by how many seconds in a day yesterday and so today and so tomorrow, but how much more? Nobody knows!

So, with each breath you take in and let out, thank God. Breathe the breath in with [gratitude]({{site.url}}/words/gratitude) and out with joy as you take the next one in, and the next, and the next, ...