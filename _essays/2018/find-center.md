---
title: Find your center
date: 2018-10-14
themes:
    - stay centered
    - lifestyle medicine
description: |-
    Everyone can and should be emotionally stable - be and stay centered. How do you find your center?
cover_image: '/uploads/essays/2018/meditating.svg'
footnotes: |-
    ### Materials Reviewed
    - [Development of emotional stability scale](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3105556/){:target="_blank" rel="noopener"}
    - [Neuroticism](https://en.m.wikipedia.org/wiki/Neuroticism){:target="_blank" rel="noopener"}
    - [8 Things Emotionally Stable People Don’t Do](http://www.marcandangel.com/2015/03/18/8-things-emotionally-stable-people-dont-do/){:target="_blank" rel="noopener"}
    - [How to make stress your friend](https://www.ted.com/talks/kelly_mcgonigal_how_to_make_stress_your_friend){:target="_blank" rel="noopener"}
    - [3 Keys to Emotional Stability](https://stephenguise.com/3-keys-to-emotional-stability/){:target="_blank" rel="noopener"}
    - [Mindfulness Improves Emotional Stability, Sleep](https://psychcentral.com/news/2013/03/08/mindfulness-improves-emotional-stability-sleep/52351.html){:target="_blank" rel="noopener"}
    - [7 Signs of Emotional Wellness](https://www.huffpost.com/entry/emotional-wellness_b_3722625){:target="_blank" rel="noopener"}
    - [Social support: Tap this tool to beat stress](https://www.mayoclinic.org/healthy-lifestyle/stress-management/in-depth/social-support/art-20044445){:target="_blank" rel="noopener"}
---
When you wake up in the morning, first thing, sit up in bed in full-lotus (or whatever pose that allows you to be) and be in symmetry with the fluid of the room air you’re held in. Bring attention to yourself, your body, your day, your breath - that you’re breathing. Do that a few minutes, in stillness, in utter calm. Follow it with heartfelt prayer to God, with deep gratitude.

When you go [22]({{site.url}}/what/), take a pose that leaves you [centered]({{site.url}}/words/centered/) so that when you eventually become [cataleptic]({{site.url}}/words/catalepsy/) in it, completely surrendered, as if plastered to the surface that holds you in, you’re in perfect unshifting symmetry.

If you feel a strain on a side or part, you know you’ve been leaning too much, too hard - it’s time to re-center.

### Wait, there’s more...
When you walk or jog or ride or whatever physical activity, do it in a centered position - keep orienting your gait and motion and awareness of motion, your attention, toward where you feel in perfect alignment with surrounding motion or energy or movement.

When you stand or sit or lie to work, at home or office, keep adjusting your stance and work tools and surrounding physical objects until you are in symmetry with them all.

When you drive, keep orienting your car in the middle of the lane you’ve picked to be in. Keep it there for as long as each path of travel allows practically. Even though you’re on the left or right of the front cabin, feel centered there and operate from that symmetry and comfort. If you’re riding adjust your perch to feel you’re in the middle of the spot as much as you got room for and is practical to be in.

Whatever you’re doing, take a stance that leaves you centered - consciously - no more weight or strain on the one side than the other. Because as soon as you feel the one side is overborn, you got to readjust, to balance again.

### Why're you doing this?
In all this adjusting, you’re finding your center. You’re aligning your awareness of self with your physical presence and in doing so, you’re balancing emotions. You're stabilizing any emotion that may either go too much north or too much south.

Emotional stability is why you should keep centering yourself - you want how you are physically to match the emotional stability needed for good health. For optimal health, your emotions need to be on an even keel despite stress, despite pressure, despite provocation, yes, despite loss. You’re neither set “on fire” with bliss or anger, nor dropped “in ice” with phlegm or depression.

Know that stress, the activator, is hardly the problem - but how you deal with stress is. Really, the stress isn’t the stress but how you deal with it is. And as long as we live, we’ll be stressed, be activated, be provoked - by people, by nature, by ourselves. We will suffer loss(es), but emotional stability is why we won’t suffer heart disease or be depressed or die because of stress.

And, yes, everyone can be emotionally stable. Everyone can be and stay centered - genetics not counting.

Are you emotionally stable? Have you found your center? Let’s see first what’s happening when you’re emotionally stable, so you could confirm. Then we'll look at how you become so and keep practicing emotional mastery.

### When you’re emotionally stable
1. You’re “psychologically resilient” - you can bounce back from negative emotions, balancing in short enough time for stability to count for you. Even if you have a ‘low response threshold’, say you're so genetically predisposed, so that you’re easily upset, prone to anxiety, quick to panic, or get fascinated, you don’t act out this “low” but have learned to recover quickly.

2. You’re unfazed by the problems of life because you have a “balanced way of perceiving” them. You also have ready emotional and physical response(s) to problems. However reality manifests itself, you're prepared for it. So, you have a generally positive outlook - if you fail, it’s due to 'manageable circumstances, not a personal flaw.' 

3. You’re not “conceited, quarrelsome, infantile, self-centered and demanding” because you have the needed “independence and self-reliance” to thrive. 

4. You get along with others when you’re needed to in order to achieve a common purpose even though you’re naturally introverted. Because you’re “trusting and altruistic” and you collaborate for the good of others, even you. 

5. You don’t have blind faith - in God or whatever else the religion you submit to imposes on you. You take realistic actions for your own safety. This is not to damn faith in a supernatural being such as God. But such faith doesn’t limit your action or make you believe in “possible” without presequent action. Really, you accept that God gave you freedoms and autonomy - tools to operate this life of yours.

6. You calm down fast and can “think clearly and remain focused” despite pressure. You can be likened to some placid lake that just goes calm and steady, serene, as if it’s never disturbed.

7. Same way you can’t be caught in a violent act, you don’t cheer at those making a sport of it. No boxing or wrestling - any violent sport - you can’t be entertained by those… You have so succeeded in managing your “disruptive emotions” - everyone possesses them - that you can’t stand scenes of violence, even if it’s called “sports”. 

Can you confirm? Do you see yourself in those? Are you emotionally stable?

### How do you become emotionally stable?
An easy way is as I listed at the outset - keep adjusting your perch or pose toward center, where you’re in symmetry with your surrounding motion; keep recentering. Now you’ll see that as a practice, what you have to do each time to be emotionally stable is to have a ready counteraction for a provocation or disruptive emotion.

1. __Pick the positive perspectives:__ Yes, every disappointment is a blessing! No, it’s not wishful thinking. It’s not another readymade pacifier - or maybe it is. But it’s a way to look at all the perspectives in a situation. 

    You’ve locked yourself out of the apartment because you jammed the door with the key left inside. The only way to get back in is to break the lock. Well, now you can finally engage your local handyman. But this may cost you much and you hadn’t a budget for it! Well, now you have an opportunity to thrive on a tighter budget, see how it feels to save or give more or appreciate how people cope who have only this way to live. Or who in the neighborhood were you able to have a decent conversation with because you couldn’t go in on time? 

    Shifting your perspective to a positive one to offset the negative we’re more naturally inclined to is one way to [stay centered]({{site.url}}/themes/stay-centered/). Even when the problem is worse than just losing your keys, there’s still a lot of positives you can find from it to help you cope with it.

2. __Build a rich store of counteractions:__ I believe very much, if not all, of life is predictable, except the timing of things. So, if for every hit you receive you already have a prepared response that leaves you steady and even, you won’t be taken off-guard or lose your calm - even when your response is a fight, a flight, or a strike when you judge you must strike to avert a disaster. 

    Knowing what to do gives you a feeling of being in control and “reinforces your emotional health and stability”. So, be prepared. Ask “what-ifs” and ‘roll with the punches of life’. 

3. __[Do 22]({{site.url}}/how/), be more mindful:__ Because finding your center, staying centered, being emotionally healthy and calm is the very reason I’ve kept at [22]({{site.url}}/what/), I won’t belabor the point here. Doing 22 is how you are mindful and you become known because you become one of “stable emotions”.

    University of Utah researchers ‘discovered that individuals who describe themselves as being more mindful have more stable emotions and perceive themselves to have better control over their mood and behavior throughout the day’. Clearly, psychological resilience is a definite result of mindfulness.

4. __Create and/or choose your environment:__ Whatever society you live in, don’t you see that the world around you is already so fast-paced you can be almost certain that you’re not keeping up? But do [let yourself be]({{site.url}}/be/) overtaken. It has to be a deliberate choice you make to go slow and you have to keep finding the space that puts no pressure on you to do otherwise.

    No, you don’t become action- or risk-averse. But you don’t react to the world and its demands either. You give yourself time to think through situations and challenges and take appropriate actions, actions that leave you sane and calm and happy.

5. __Practice compassion daily:__ Compassion allows you to sympathetically be in other people’s consciousness. As well as help you to alleviate pain for others, compassion can also help slow down your upset/angry emotions as you can see why a neighbor may “unjustifiably” scream at you, so you don’t just get even so. 

    When you know you owe it to people to treat them well, your first reaction when they offend you will not be to get back at them but to help them. People who’re emotionally unstable may actually be helpless and you can’t help by “paying evil for evil”. But if they seem recalcitrant to help, you just let them be. You move on - choose your (loving) environment.

6. __Keep mindful of your life’s purpose:__ Building [a company around the practice of 22]({{site.url}}/about/) as well as publishing [well-researched content]({{site.url}}/essays/) for free is part of my mission to create the kind of world I want to live in - where people are emotionally healthy and calm. Being mindful of that mission means there’s a whole lot people consider normal human reactions that I have to avoid. I don't want to excuse rashness for "you know, I was angry..." No.

    Not to say that you won’t feel tough emotions, but you won’t let them detract you from your mission or make you out to be a hypocrite - only saying, never doing. 

    Keep doing what you connect with and care about, which is [what is happy]({{site.url}}/happy/), which can help others and hurt no one, and let the meaning and self-fulfillment and joy you derive keep your emotions on the even keel.

7. __Be a, give, and get social support:__ See social support, whether emotional or tangible, as a two-way network - you give and you’ll be given. Notice, I refused to say “take”?

    You shouldn’t give just so that you can take. But because everyone is giving in the social space you pick to be in or you build yourself, you’ll be able to take. When you care for others and know you’re cared for and will be cared for, you become more emotionally stable.

8. __Take lifestyle medicine:__ Lifestyle medicine is a cocktail of diet, exercise, stresscare, and social support. Taking lifestyle medicine means that you eat plant-based, wholegrain, mostly unprocessed foods; you exercise approximately 22 minutes daily; you don’t smoke or befriend smokers; you don’t consume alcoholic beverages; you “make stress your friend”, meaning you care for it, not “believing that it’s bad for you” but being aware of it and taking steps to reduce its causes.

    As for causes of stress, each of us will do our own self-examination to see how our body responds to each thing we do - whether mental or physical - then know how much we can tolerate. Thus, we will not be taking on more than we can stand.

    For the fourth ingredient in the lifestyle medicine cocktail, you give and be given social support. It’s just as I have shared above in “how” #7.

### Your center
I’ll close by saying that your center is where you’re emotionally stable. It’s that place where your physical stance and composure match their emotional equivalents. 

You find your center by taking steps, such as the ones we’ve considered so far, to master and keep emotionally stable.