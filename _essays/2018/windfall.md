---
title: When your windfall comes
date: 2018-07-07
themes:
    - manage money
description: How can you not lose your calm despite a windfall? How do you discipline yourself to match your newfound money?
show_notes_form: true
notes_form_cta_text: How'd this essay affect you? Please share with me. I read every comment, and you might see yours in an update. Thanks.
updated_date: 2019-07-23
last_modified_author: Kabolobari Benakole
---
What may be windfall to the one person may be just add-on to another. Yet, everybody at one point in their life, if not from time to time, gets a windfall.

### Your windfall
You make manager at your company and a brand new car is added on top. You get a performance bonus. You get a large cash gift amounting to 500% what you’d earn in a whole year. You win a (life) insurance proceeds. You cash out of a business/startup in excess of millions. You receive an inheritance. You win a large lawsuit. Or a lottery, the list goes on.

For the long term, a windfall can completely change the course of your life - for good or for bad. But before long you would’ve been a train wreck because being human you let your emotions take over.

### Typical reactions
Your excitement becomes bliss then irrational exuberance then hysteria. You go on a buying spree, giving yes to every want, every [ask]({{site.url}}/ask-why/). You drive recklessly. You have wild parties, drinking matches, illicit sex, orgies.

For others, it may be loud uncontrollable long-winded talking everywhere they go, and they now just go everywhere, unplanned as they do. Boasting along with it. Condescending to everyone. Voiding of self-time and rest and meditation. Every piece of action unbridled. Every want sated.

### Can you be tamed?
How can you not lose your calm despite a windfall? How can you keep completely under control, [carefree]({{site.url}}/words/carefree/) - [the middle way]({{site.url}}/middle) - and develop discipline to match your new circumstance?

I’ve put together this guide to help. It’s a collection of 6 principles, more like “accepts”, that your new situation should warrant, giving you adequate preparation for the sudden turn of events.

Do note that this guide is not to show you how you ought to spend your windfall. I’ll leave that to your wealth or financial advisor. More crucially, this guide is to help you deal with _you_ in spite of the new wealth, not letting it get the better of you. Of course, if you proceed with calm, you’ll spend the newfound money wisely. Good for you.

This guide should also help you if you’re yet to receive a windfall because everyone receives a windfall some time. When your windfall comes,… let it be sudden, but your actions deliberate.

### 6 accepts to a windfall
1. __It’s possible.__ Accept that it’s possible - don’t let yourself be surprised by it. It wants to give you shock because your brain has hastily calculated the percentage difference it makes. Pause the math. Say it’s possible, it’s just fine and normal, that it was meant to be anyway. Let you, not it, be the one in charge. Nothing should manipulate you.
2. __You deserve it.__ Accept that you deserve it. Don’t start with “O, no, this can’t be happening to me!” Immediately get to work recalling how you got here, the things you did. I mean, if it’s called “performance bonus”, you performed! So, you deserve it. But, if it’s a lottery win and you neither want to disclaim nor wholly refuse it, then you got to see it as a deserved “luck” in your favor. If you deserve it you’ll take care of it.
3. __It has a purpose.__ Accept that there’s a purpose or purposes for the windfall. Never let a minute pass when you thought it was pure accident, “luck”. Believe it was meant for something or somethings and now it’s time to accomplish that purpose or purposes. As the 5th accept will show, take your time, a long one, yeah, to get to this.
4. __It has a place.__ Accept that there’s a place for it - not your lap, not your bank account - or find a place for it. Do this because if you can’t, no matter how grand the purpose, you must refuse the windfall. Am I crazy? No, but I must avoid being in a worse situation than before I’d received this windfall. I’d be better off not taking it at all! By place, I mean both mental and physical - you may either be ready and accommodate a windfall or let it go. Again, I’m not crazy.
5. __Take your time.__ Accept the gift of time, take a lot of it - as long as the windfall is large, even months, a lot of 22’s [doing nothing]({{site.url}}/thinklist/nothing) and deliberately letting it simmer then cool. This abundance of time you give yourself definitely allows you to make sure of the first 4 accepts. I believe if you’ve been living a life of calm you will be prepared enough so that within a couple minutes, perhaps hours or a few days you’ll figure if a windfall is for you or not in order to disclaim it.
6. __Remain you.__ Accept the way you are - don’t try to make quick life- or situation-altering decisions. I could say, don’t change. Don’t imagine “new life”, “new level”, “new me”, “yeah, I can finally be like them”,… That now you can send your children to the same schools as their children, join the clubs they’ve been a member of, hang out where they hang out, match them in car/home ownerships,... you get the idea. Really, if you’ve lived a life of calm and minimalism, you’re enough, you’re pleased with your enough, you’re not comparing yourself to them at all, thus, you’ll do nothing for the sake of that.

   This windfall can’t change you.

Do you see that these accepts can be lifesaving? Can they make up points of preparation for you when? I’m not sure you can go wrong with any windfall if you take these to heart.

Yes, when your windfall comes, let it not change you.