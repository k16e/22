---
title: Always ask why
date: 2018-08-04
themes:
    - increase focus
description: |-
    My resolve is, if you can’t find a reason for it, it may not be worth your time or effort or action.
---
If you’re already doing [nothing for the sake of it]({{site.url}}/nothing/) you’re doing everything for life and nature. That is, “according to life and nature." Always ask “how does this assist life and nature?” “Why should I do this?” Is this [happy]({{site.url}}/happy/)?

Before anything. Ask why.

Because arbitrary is only a word, a shortcut. A dubious excuse. No, nothing is arbitrary. 

Truth is, when you hear (or say?) “I just wanna have fun… for nothing, just for the fun of it…” it is, really, ignorance or, worse, pretense. You either know why and won’t or shouldn’t admit it or you don’t and don’t want to [contemplate]({{site.url}}/words/contemplative/) or investigate it. Or you relinquish your patience to ask why because the desire for “that thing” is “too strong”. The reasons why you won’t admit ignorance or knowledge remain, well, best known to you.

My resolve is, if you can’t find a reason for it, it may not be worth your time or effort or action. You may have to let it go till you can understand why. And keep asking.

How this is freeing - both knowing why and doing or not doing and letting go to do later or never - is why this “ask why” experiment is vital to your calm and a worthy subject of [22]({{site.url}}/what/).

### Ask why: an experiment
Don’t worry about spontaneity. Everything has a getting-used-to-it curve, where at some point, without asking why you could still take actions unregrettably - you’ve gotten used to good, the virtuous, the peaceful, the calming. The better choice.

### Make it practical
Next time you desire your favorite bourbon whisky, [go for it]({{site.url}}/enjoy/) - it pleases your senses, delights your soul, heck, it’s even got some good bacteria. So, there you got a why, a couple. But, if you were to take alcohol just for the sake of it you would over drink - drinking too much or drinking all the time. Your soul and senses do not always desire or need the sensibility! 

It’s similar with dance. You love to dance? Why? What’s dance do for you? Why should you dance? How should you dance? Where should you dance? With whom? To what music? What rhythm? Then go dance. Have fun. But dance for what dance does for you, not for dance's sake. 

And, when you got to dance and you pick someone to dance with, will it leave you happy? Them happy? Everyone happy? [Is it happy]({{site.url}}/happy/)? Should you dance with a spouse not yours? And how should you dance? How close? How long? What happens after?

Ask away. Ask why. Keep at it.

{% include components/break.html %}

Would you become obsessive? Edited? Predictable? Mechanical? Programmed? Lack spontaneity? 

I don’t think so. 

Really, the things you do when you feel or know you should, do add a touch of surprise, spontaneity to your life. But just doing without bothering why always comes back to bite you. Alcoholism, casual sex, unwanted pregnancies, wrongful abortions, drug abuse, burnout from work, preventable deaths, the list goes on. It always comes back and bites you.

Your life will continue fun because even when you’re automatic, you’ll be doing a good thing, having grown used to the good reason why. [Hold life and nature in mind]({{site.url}}/words/teleology/).

Always. Ask. Why.