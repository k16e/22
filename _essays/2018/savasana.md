---
title: 22 or savasana?
date: 2018-08-11
themes:
    - stay centered
description: |-
    I can't help noticing that my favorite pose at 22 is a savasana. So, is 22 just savasana?
---
Wrapping up the session of a particular yoga video I watch, the instructor says “savasana,… I’m sure you’ve probably said before that you want peace and quiet… now you have it…” as her asana partner lies flat on her back, heels spread as wide apart as the edges of the mat “be with it… give yourself this gift of doing nothing, of pure rest…”

She and her partner keep still in that pose for the remaining few minutes of the session - about 2. 

Savasana, I thought, the first time I heard it, “what’s that? 22?” I didn’t look it up that day.

But day after day, each time I do 22, I can't help noticing that my favorite pose (or asana) is typically (or corresponds to) a “savasana”.

Is 22 then savasana? Therefore, isn’t 22 one kind of yoga?

### Is 22 savasana?
Hmm. 

From what I’ve so far studied and practiced and known about savasana, I’m eager for yes - 22 is savasana. But... not exactly. Well, it helps to look at how both practices are similar and yet different to get why not exactly. Plus, yes, you can savasana at 22 but you can have a few more [thinks]({{site.url}}/thinklist) (real mental activity that still leaves you restful) at 22.

### 22 & savasana

| Savasana | 22
|--- |---
| Lie and look dead | Lie and look dead
| Total surrender, body to become slow and heavy, as if sinking into earth, as if to make an imprint in the surface | Total surrender, body to become slow and heavy, as if sinking into earth, as if to make an imprint in the surface
| Deliberate rest, you can “power nap” with it | Deliberate rest, you can “power nap” with it
| Total and relaxed stillness, no movement, no scratch no itch - no touching no tingling sensation | Total and relaxed stillness, no movement, no scratch an itch - no touching no tingling sensation
| Assume release of consciousness | Assume release of consciousness (sometimes)
| Before, between, after asanas | No yoga needed, can be taken anytime
| You must stay awake | You may sleep, you may go into trance, feeling hypnotized
| Hover between sleep and wakefulness | Don’t fight sleep, sleep if sleep demands. Really, 22 can be used to [sleep]({{site.url}}/sleep)
| Just another yoga asana/pose | A way of life, imbibing calm, more to 22 than the few minutes of stillness
| May be accompanied with music or mantra or guidance from a teacher | Best when not paired, just its own, just you, attention only to you or [nothing]({{site.url}}/thinklist/nothing/) - you learn to be then you [go be]({{site.url}}/be/)
| Insists on nothing every session | You’re allowed to have one [think]({{site.url}}/thoughts/) during session. 22 can be used to [build knowledge, making sense of realities]({{site.url}}/thinklist/understand/).
{: .table-striped .alt-thead-bg }

{% include components/break.html %}

So, is 22 savasana? Yoga? No and no. But it can be practiced as yoga.