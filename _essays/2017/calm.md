---
title: 'Calm: a key benefit of 22'
date: 2017-06-20
description: |-
    Besides 6 or 7 other benefits of 22, calm in itself as the hallmark of the practice, has its many benefits too.
---
Since I discovered calm after [my first (and hopefully last medical) MRI]({{ site.url }}/mri), I’ve been helped in several ways. 22, being a sort of MRI, produces calm - a key benefit, a hallmark. 

While I could say having calm has benefited me in several ways, it seemed rather tough for me to fully frame those ways and do an essay that you can learn from. So, I researched and found a few things that match what I’ve since felt. I've previously presented six other benefits of 22 in [this other essay]({{ site.url }}/why).

### Calm and discernment
Discernment has been defined as a quality or ability one may have that helps them easily “get the point”, grasp an otherwise obscure or difficult concept. _(Merriam-Webster)_ 

It’s associated with wisdom and sagacity. With discernment, you can make good decisions, accurately predict people’s motives and character, so can make true friends, and can see what is not evident to others. Great stuff, don’t you say? 

But discernment comes from calm. This is attested to first by the Bible. It says at Proverbs 17:27 that “a discerning man will remain calm.” Absolutely! I think I’m more discerning now than before I started 22. Taking these moments off builds in you the discerning quality.

### Calm is enlivening
There’s such a thing as feeling rich - full of life. Yes, I feel this freedom in the head, this clearing, this well-being, this general joy - apparently over nothing. But it’s offshoot of calm. I’m enlivened, that is, I breathe freshness everyday.

> A calm heart gives life to the body. <cite>— Proverbs 14:30</cite>

With calm as aspiration and achievement, life is fresh flowers in a room always.

### Right decisions & sound actions
When you’re calm you can make meaning, which helps you make the right decisions and take sound actions. 

Your brain gets "an opportunity to pause amidst the chaos, untangle and sort through observations and experiences, consider multiple possible interpretations, and create meaning. "This meaning becomes learning, which can then inform future mindsets and actions.” More in [this article from HBR](//hbr.org/2017/03/why-you-should-make-time-for-self-reflection-even-if-you-hate-doing-it){:target="_blank" rel="noopener"}.

### Cut cortisol, destress
[As science shows](//www.verywell.com/cortisol-and-stress-how-to-stay-healthy-3145080){:target="_blank" rel="noopener"}, cortisol is a good hormone that helps to keep us immune, less sensitive to pain, and homeostatic. But higher and prolonged levels of this good guy in the bloodstream backfires on us. It decreases our immunity, make us more sensitive to pain, shoots up our blood sugar levels, and leaves us fat and sick. 

Calm is the surefire way to maintain healthy cortisol levels in order to return the body to normal after high stress or hyperactivity.

### Might there be more?
You bet! I dare say the benefits of calm - the other word for 22 - is endless. 

As I experience more I’ll share them [here]({{site.url}}/essays).