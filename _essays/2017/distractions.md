---
title: 10 distractions to 22
date: 2017-07-01
description: |-
    Because 22 requires a state of total surrender to the moment, such distractions as listed in this essay should be avoided during the practice.
---
The following distractions were already clear to me as such from once I understood [the exercise of 22]({{ site.url }}/how). I mean, [the MRI]({{ site.url }}/mri) itself had revealed a lot. 

But all this time I hadn’t thought of listing them out as a separate [Update to 22]({{ site.url }}/essays) for reference purpose. Well, I have now, so take a look. 

At all cost, these should be avoided during [22]({{ site.url }}/what). 

_(I understand that depending on where you work and may choose to do 22, some of these distractions can’t be eliminated and must be tolerated. It comes down to [calm]({{ site.url }}/calm) - let nothing disrupt the serenity of your 22 time._

### The 10 distractions
1.  **Sights:** Shut your eyes during 22. You see nothing, you do nothing. It’s a trip you should enjoy. Alone.
2.  **Itches:** Scratch none during 22. They’ll come every now and then, and titillating so, but scratch them not, lest you’re distracted. The pleasure that scratching an itch brings is not the one for you at this time.
3.  **Strains:** Take a comfortable position to do 22. You don’t want to be strained at some point or joint and burn to move. It can be very tempting to let go. I know.
4.  **Thoughts:** Focus on [none or only one]({{ site.url }}/thoughts) at 22.
5.  **Reasoning:** Do not try to understand or make too much sense of the thought you focus on, just dwell on it for as long as 22. But note the clarifications that come - hold them somewhere because you’ll journal them [after]({{ site.url }}/after).
6.  **Technology:** Go airplane mode on every electronic device during 22 - no phone calls, no emails, no Internet. Nothing but you and your one thought or none.
7.  **Lights:** The dark is most suitable for 22. Darkness keeps you fully isolated. Really, seeing yourself can itself be distracting, in case your eyes opened accidentally.
8.  **Fill:** Eat light because too full stomach detract from the kind of focus needed to enjoy 22. Too hungry may also distract - except you have developed that [solid endurance]({{ site.url }}/why) of hunger. Except you really can't access food, should you starve? Don't get me wrong, fasting has its place, but that's another talk.
9.  **Noise:** As far as and as much as you can, avoid all audible stimulation while at 22 - not even smooth jazz cuts it.
10.  **Movements:** Both the ones by us and around us, avoid all physical movements to derive maximum benefits from 22.