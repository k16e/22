---
title: Make every siesta a 22
date: 2017-07-21
themes:
    - sleep better
description: |-
    Both in the office and on the road I've practiced putting myself to sleep by deciding it was time to do so. Sleep is indeed discipline.
footnotes: |-
    ### Does the mind sleep?
    I no longer think that the mind sleeps. It never does, even during the deepest of sleep. I think the mind sleeping would be absence of consciousness, even death. 

    What I accept now is that the body can let the mind be and go to sleep. By that I mean, the control we have with our brain, we can pause its action on the mind's movements. We temporarily halt thinking and [go "empty"]({{ site.url }}/thinklist/nothing/), go limp, [letting ourselves be]({{ site.url }}/be/). Thus, we sleep even if the mind continues wide awake rampaging because consciousness. We sleep, we dream, and we're refreshed.
---
I have said [“sleep is discipline”]({{ site.url }}/sleep). I mean you can always discipline yourself to sleep. Since I said it, I keep looking to make it true. One way I've done so is with siesta. 

Sleep didn’t used to come easy to me. While [22]({{ site.url }}/what) has changed that since I'd started, it isn't been all that straight-off. One should be able to lie down on bed and say “it’s bed time, I got to sleep” and sleep, right? But, if you’ve been [insomniac]({{site.url}}/sleep) or have struggled with sleep, you know it’s far from reality. With siesta I think I’ve proven the “sleep is discipline” thing. 

I've chosen to be 22 in the office and then on the road. When my alarm woke me 22 minutes later, rebooted, refreshed, I realized I’d succeeded in disciplining myself to sleep.

### In the office
Since I’ve been employed, I’ve always taken the liberty, confident at it, to take a nap right at my desk middle of the afternoon. I do this because of the understood health benefits, even as the restoration of energy it brings makes it a tempting bargain. 

However, what happens on several occasions, to the point of my going several months without siesta, is you’re so filled with thoughts of work to do you imagine taking a short break in sleep is just lousy, unserious - a waste of time. I would panic, heart beats racing, pit of stomach drumming, the moment I tried to siesta. Then I’d just sigh and get back to work. Isn’t that what everyone does - get on working? 

[But 22!]({{ site.url }}/why) On this afternoon, I thought, “sleep is discipline, right? Why not? Do 22 and sleep!” I did. And I slept til my alarm went, 22 minutes later. 

I’ve called it “disciplined resting”. You see, 22 stills and rests the body so it has [no choice but to sleep]({{ site.url }}/sleep). I should say stills and rests the mind, too. Because if [the mind]({{ site.url }}/calm) doesn’t sleep, the body never. From this successful trial it wasn’t difficult to take a pause on the road when I felt the urge to siesta. It was time for it.

### On the road
If you drive you know the instruction to always pause a few minutes in a nap to avoid sleep-driving. But who ever does that? I play this in my head all the time even while only a quarter awake at the steering. But I’d never pause and rest. I’d just plough on until the sleep would clear. 

It’s what every driver does, right? Until this afternoon when the urge came and I know I could yield because remember, “do 22 and sleep?” So, I drove into a refueling station off the highway and parked, leaving my windows wound up, it had rained, the wheather was still cool. I 22’ed and guess what? I'd again disciplined myself to sleep. I'll keep at this.

### Disciplined resting
This disciplined resting that 22 brings is working for me. I urge you to try it. Once it’s sleep time, especially siesta, because that’s the time when amid piles of work, you feel guilty if not unable to take a few minutes off, do do 22 and sleep. 

When you know you can’t move your body whether lying or sitting with your head down on a table or in whatever other comfortable position you can take, you’re forced to slump and that’ll be it. 

I feel certain that with 22 I’ll always sleep when I want to or have to.