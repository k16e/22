---
title: Finding time to do 22
date: 2017-07-08
description: |-
    Finding time to do 22 even once a day is a big deal for most of us. Yet, I recommend having it three times daily. How can we do that?
---
Should I prescribe, I’d say do 22 three times each day - morning, afternoon, and at night. An entire hour of repose each day! A lot of time for most of us. Add [the moments after 22]({{ site.url }}/after) I recommend we remain offline to document clarifications, inspiration, or just make sense of the session, and we may have from 3 to 5 hours of utter withdrawal each day. So much offline time. 

This is certainly not possible for most of us. Really, finding time [to do 22]({{ site.url }}/how) even once a day is a big deal. So how do you do that? Some suggestions. But first...

### Not time to do but benefits from doing
22 has [a lot of benefits]({{ site.url }}/why) - mentally and physically. Besides the virtues of discipline, endurance, and joy, we can [sleep better]({{ site.url }}/sleep), have [better memory]({{ site.url }}/memory), be more intuitive and discerning, [better heart]({{ site.url }}/calm) - free of diseases. [Be happier]({{ site.url }}/happy). 

So, whatever your work schedule is, you can make time for 22 if you think less about the commitment of time but more about the benefits of the practice. If cognizance of these benefits has you bound, you see this as important. We make time for whatever is important to us. 

Nonetheless, some of these suggestions may help you find time.

> By all means, have 22 at least once each day.

### #1: Let your people know
For me, I started [this channel]({{ site.url }}/essays) as a way to let others know [what I’d found]({{ site.url }}/mri) and will be doing. 

In the office, I shared 22 with teammates and close colleagues. I explained how I wasn’t going to miss [siesta]({{ site.url }}/siesta) anymore, that I was going to make it 22. Incidentally, siesta time makes my second 22 session of the day. I’ll come back to this down the essay. 

At home, among family members, I continue to make everyone I associate with get used to my routine, including my offline moments - I take a lot. So, whether you’re employed or self-employed, let the people you report to or care about know what you want to do and why. Then they can let you have this time or, at least, tolerate your having it.

### #2: Don’t cave to not meeting up
There have been days when I’ve missed 22 all three scheduled times. Days when I was needed for “urgent” work 6am and pressed on till midnight. Days when my 22 was abruptly terminated - yells of my name, knocks on the door... 

Yes, you would’ve started then work pressure or other of certain days make you miss. Don’t give up. As you press on, keeping [the goodness of it]({{ site.url }}/patience) ever in focus, you’ll soon find yourself naturally falling into it, as if people just [let you be]({{ site.url }}/be). 

This has been my case. _“He’s doing his 22”_ was my colleague's voice to other ones in the hallway when they came looking for me - it was noontime. In our common lodging, I had my DO NOT DISTURB sign read “22 minutes” with a sticky note. 

Incidentally, Reminders _(coming soon)_ will have DND-22 door signs. 

Nothing beats the feeling of “I can always relax.” You’re at ease. And you know what, you’ll love yourself for the respect people give you for being this disciplined. Glow. Go for it!

### #3: Inter your intimidatedness
Except when employment only wears a veneer of modernity, masking old-school slave-labor manners, employers know that 1 hour out of your 9-5 is yours. Don’t be intimidated if you're “caught” napping, slacking, or best if doing 22. Meditating. Being mindful. If you already did the #1 thing, letting them know, you should be good.

> Just tell your boss, “I was doing 22” and you’ll be good.

Inter your intimidatedness because, truth is, everybody accepts that 22 is good. They just can’t be ‘caught napping’, yet. I find people at different levels of optimal living. Just see them as yet to catch up to you and hope they will. Soon.

### #4: Don’t be guilty for detaching
People love to work. Yet, some people are forced to work for money, fear, respect, or for a ton other reasons. For all these people, detaching from work for a few moments can be incredibly (if not ridiculously) tough. But the benefits of the restorative power of the detachment of 22 do outweigh whatever guilt you may feel to detach and/or whatever backlash you may get from coworkers, sups, or bosses. 

The advice here is to have the benefits of 22 always in focus, weigh them for yourself against the opposing forces, and be prepared to give an answer to those who may have another opinion. It’s really about you, not them.

### #5: Postpone 22 for urgency
But where the work is urgent, an un-give-up-able stretch, don’t hesitate to postpone 22 to a more convenient time. Then again, never beat yourself for doing so. Really, if you’ve been doing this you should be calm enough to not let a small sin bother you. 

As for that convenient time?

### #6: Make 22 a morning or evening routine
The one time maybe no one can steal from you is your morning. Maybe your evening too, when you’re back from work. Not even your spouse should. So, definitely start the morning with 22 or wind the day down (say, disconnect) with it. 

When you have mastered these times as your fixed schedules for the practice, even when you miss it at noon, or say on an evening, you’re happy you had it in the morning. Or if you miss the morning, then noon, you determine not to at night. 

By all means, have 22 at least once each day.

### #7: Keep backing into balance
You might need to always have in mind that work, no matter how much you love it, which in itself may be counterproductive, will always increase your stress levels, break your resistance to stress-related conditions, and burn you out. I’m serious. 

Whatever you do, as long as it can be called “work”, don’t love it so much it hurts you - it's a cancer. 

That in mind, you must be very deliberate about getting you and your body back into balance regularly. Only you can stop yourself, and 22 is that one schedule item to help you do so.

### #8: Love yourself Very much
Have an almost brash confidence in your own physical features and abilities. Glow in the admiration from people - there must be at least one person who admires your intentions. Look for that one person and enjoy their love. They know you do 22? Excellent, it’s why you keep doing it! You know why; you can’t stop. You can’t be stopped!

### #9: Use your lunch hour (and siesta!)
Wherever you work, even when you’re the boss, give yourself at least one hour - your lunch hour and take 22 minutes off of it for your emotional well-being. The [need for siesta](http://alifeofproductivity.com/productivity-experiment-take-three-hour-afternoon-siesta/){:target="_blank" rel="noopener"} cannot be overemphasized and I’ve found the suppression at 22 to be perfect for letting you sleep.

{% include components/break.html %}

Yes, [sleep more]({{ site.url }}/sleep) - it’ll help you do more, do better. 

Finding time to do 22 may be daunting. But, remember the benefits - they’re about you and only you.