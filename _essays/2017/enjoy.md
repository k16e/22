---
title: Enjoy - a philosophy
date: 2017-07-23
themes:
    - stay centered
description: |-
    It's the time for each thing we must enjoy that makes the difference. Desire itself for a good thing isn't bad. But desire it in its time.
---
Are you hungry? 

What do you want to eat? Or drink? What smell do you want so close, to smell, to drag long in and exhale? What? What does your tongue want on it? To roll up? What’s making you salivate? What do you want to water your mouth? Enjoy it. Enjoy that very thing. Go for it—if you can afford it. Yes, do so. 

For God gave you a tongue, a discrete one for that matter that can tell a thousand tastes apart. A set of nostrils… O, no, not those. Not the tongue, either. Not the eyes. 

Yes, a brain that wants delight for your soul. For your life. This is good. Or, this is not so good. To keep your mind at ease, [to calm you]({{ site.url }}/calm). Because when you get what you desire, you calm down, so go for it. Enjoy.

> Desire for a good thing isn’t out of place.

### What Am I Getting at?
That whoever told you - whether you did yourself or assumed or actually heard or read - that [a life of calm]({{ site.url }}/why), of seriousness, of mindfulness, of being deliberate, of high-consciousness, is boring told you a lie. Or they're completely ignorant they got a lie. 

Let me bring you some [teleological thinking]({{ site.url }}/words/teleology) for a second. Your eyes aren’t where you have them for no reason! Apologies to the unsighted. But somehow, just somehow, we can all feel what is good, can picture what is good, can visualize it, are delighted. We may be disabled in one form or other…but somehow, someway, we get delight… 

Desire for a good thing isn’t out of place. For when you want to eat or drink, please your senses and feel good with yourself. But know that there’s a time for that. It’s that time that makes the difference. The time for that thing. The time you should have it. [You can’t indulge an appetite]({{ site.url }}/nothing) when the body isn’t hungry or need just another drink or sex or candy or Mars bar just because the eyes are asking for it, making you crave. 

And, what’re the eyes for? Or the nostrils, for that matter? Or the tongue? 

When I walked into the store the other day and smelled vanilla off the Ahmad Tea and wanted some and went ahead and bought the pack, even if I’m not particularly into black tea, it’s the black tea one, it was the delight of tea when I should have tea that made me so decide, not some irrational craving! 

I mean you can go ahead and have what you desire but let what you desire serve a need and be for when you should have it, and not more than you need and should have. And do note that you need to enjoy what you should have. When you should have it. The time that makes the difference. 

Don’t mistake taste for craving. Don’t rhyme enjoy with cloy. Let the experience of the taste invite you into the smell, but [have patience]({{ site.url }}/patience) till when you should have it. And enjoy it then but don’t cloy on it. Have it for what it should do for you, [not for the sake of itself]({{ site.url }}/nothing) or your wanting it. 

Enjoy what you have and be satisfied that you have. Keep it for when you must have it, not because it’s desirable. Maybe it’s only in your eyes or your brain.

### Enjoy & Calm
You know what’s calming about that vanilla tea, even if I’m not into black tea? By the way, what’s it mean to be into something? See my point? You love black tea, don’t you? Well, enjoy it! 

What’s calming is that I’m having what I’m enjoying when I’m having what I should be having at the time when I’m taking tea - and it’s not all the time. But the vanilla, ah, the nostrils are always asking for it, the senses... 

Trouble is not [calm]({{ site.url }}/calm). 

If you keep this philosophy in mind of enjoying what whatever only at the time for it you will keep your calm. Enjoy.