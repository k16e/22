---
title: After 22 moments
date: 2017-07-04
description: |-
    Not every time after 22 will produce something creative or tangible. You don’t have to stress or strain it. But still don’t fritter it away. It’s yours.
---
If you can spare the remaining minutes after [22]({{site.url}}/what), even past the hour, if possible, to do something creative you’ll be the happier for it. The tranquility of those minutes ought not be frittered away. Don’t be quick to get back to the activities or tasks of the day. 

Here’s some things you could do to make the session worthwhile. Remain offline. **_These suggestions aren't an ordered list you want to do the one after the other. In my case, since I’m more inclined to publish my experiences, the "journal—think into ink” action is more of first._** I may sometimes pray and that would be it. Or continue writing an unfinished piece. Or remain in [repose]({{site.url}}/words/repose) thinking. Or just wander on. Or read. Read. Breathe. You decide what soothes you.

> I’ve found the insouciance after 22 to be most suitable for short devotional.

### Pray
Now I’m not asking you to believe in Jehovah, the supernatural personality I offer exclusive devotion. But I’ve found the [insouciance]({{site.url}}/words/insouciance) after 22 to be most suitable for short devotional. I sometimes just thank him. I ask for more commitment to calm, more relaxed state of mind, more confidence in [minimalism](http://www.theminimalists.com/minimalism){:target="_blank" rel="noopener"}, more love for me, yes, for me from me, then for him, and for youmanity.

### Journal - think into ink
Recall the clarifications or better understanding you have and write them all down. You’re thinking into ink, not letting anything go - silly or sound - sense would come later. Because you could do this and yet not have an overwhelming amount of things to journal, is another reason you should limit your 22 to [one thought]({{site.url}}/thoughts). If you’re a writer, this is good practice - you’re merely journaling. You’d refine and rewrite later. 

But why shouldn’t we all write? I mean, everyone has original personal experiences that everyone else can benefit from should they publish!

### Outline
Akin to journaling is outlining. Just pick up a sticky or something and start listing your thoughts blow by blow. [Todoist](https://todoist.com){:target="_blank" rel="noopener"} has been a good tool for me to outline - I just type in the words as arranged in and coming out of my head, letting them pour out without prejudgment, as tasks. It sure feels good, too, to check things off later!

### Deliberate more
You may use this time to deliberate some more on a course of action. For more mindfulness. Perhaps a little bit of yoga? You may do a key piece of research or an aspect of it that may have been difficult. Something contemplative.

### Write/Research
Go on straight to [Evernote](https://evernote.com){:target="_blank" rel="noopener"} or whatever tool you find comfortable writing into… Write. Start a new piece, perhaps this was [your 22 thought item]({{site.url}}/thoughts). Or continue a previous unfinished piece. Or deepen your understanding into a subject by research.

{% include components/break.html %}

Yes, these few moments after 22 can still be yours for the keeping and use and [benefits]({{site.url}}/why). Make them count. Of course, not every time after 22 will produce something creative or tangible. You don’t have to stress or strain it. But still don’t fritter it away. It’s yours.