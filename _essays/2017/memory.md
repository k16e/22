---
title: Memory is in retrieval
date: 2017-09-10
description: |-
    22 helps to exercise memory, thus building it. No luxury of science to explain this, but I imagine my recall is sharper because 22.
footnotes: |-
    ### Explanations:
    <sup>*</sup> SSCE is Senior School Certficate Examination in Nigeria.
---
An eighth [benefit]({{site.url}}/benefits) (by my count) [of 22]({{site.url}}/why) is memory. 

By that I mean that 22 enables you to recall better. 

Incidentally, memory is in retrieval - how much you can pour out when invited to, when the need to arises. It’s not in storage - how much you have up there. In fact, how much you have up there is nothing until you need to retrieve it and use it.

### 22 builds memory
You know that at 22 you [restrict your thinking to one or none]({{site.url}}/thoughts). Also, you try to have just a few strings within a think which you go over and over. 

Now, both during [the calm of 22]({{site.url}}/calm) and [after]({{site.url}}/after), you recall these things into a journal or just out loud to yourself or someone else. It’s how 22 helps you to build memory - to strengthen your retrieval. Or, in other words, helps you to exercise your memory, building it.

### There’s no better memory
I’m not sure when it comes to memory there’s one better than another. It's just practice. Everyone does have and can sustain a good memory. Yes, everyone *can* be good with names. Everyone can recite their favorite poems. Sing their favorite songs. 

And, what I’ve found is methodically thinking through things, placing them structurally in memory, then regurgitating them in that order, with studied practice, on and on and on, actually builds an excellent memory. This isn’t “memorizing” or rote learning. This is making things you care about part of you. This way, you have them when you need them. What's more calming than that?

### A backstory
You ever had a difficult exam? I remember one paper I sat during my second SSCE<sup>*</sup>. About 30 minutes in and I was blank. Then panic came. I ejaculated. Yes, ejaculating along with the sweating. It was very bad, the experience. 

I did manage to struggle some things into the paper, but I failed. Until it was over and I turned in my paper - half full, really, three-quarter empty - I didn’t calm down. I couldn’t. 

Looking back, if I knew then what I know now about deliberately calming down and memory, I’m sure my recall would’ve come. 

I’ve been through several similar situations lately, not typically exam setting, though. Calming down the incipient panic, I've remembered my learning and given answers accordingly. 

This kind of sudden forgetfulness is natural. It’s akin to the fight-or-flight feeling - it comes to everyone. But if you’ve been doing 22 or have gotten calm otherwise, you know how to bring it on at this time. Soon, you’re calm and get the resilience to deal with the test, however stressful.

{% include components/break.html %} 

Anybody can do this. Anybody is capable of apt memory, of total recall, anytime.