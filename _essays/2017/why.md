---
title: Why you should do 22
date: 2017-06-05
description: |-
    I've observed at least 6 crucial benefits of 22 as long as I've been doing it. Here's each one briefly.
---
It wasn’t long after [I conceived it]({{ site.url }}/mri) and started that I determined that [calm is the ultimate why for 22]({{ site.url }}/why). Having been doing it, I'm seeing 6 other qualities growing in me that are key to a life of all-round satisfaction and happiness. 

If you’re new to 22, [start here]({{ site.url }}/start).

Beyond calm, 22 will build in you:
1.  Endurance
2.  Self-control
3.  Discipline
4.  Focus
5.  Mindfulness, and
6.  Joy

If you know that these qualities enrich life you know why you should do 22 and be on the path to mastering calm. But let’s look at each and see whether possessing it isn’t why we live at all.

### Endurance
We’re a victim of our own culture of “social” and [“productivity"](//blog.evernote.com/blog/2017/02/15/on-minimalism-the-difference-between-focused-and-busy/){:target="_blank" rel="noopener"}. Everything around us urges us to get along, mix up, meet people - live or virtual - get things done. 

But the soul wasn’t designed to “endure” such overstimulation. 

22 affords you opportunities to pull away. These silent, soul moments build your endurance of solitude, which in turn, gives you equilibrium. Equilibrium destresses you, reduces risks to cardiovascular diseases, eases pain, and builds a positive mind.

### Self-control
The hallmark of 22 is the capacity to rein in on temptations and impulses. You'd think an itch is a small thing. But by the time you endure several titillating ones during 22, you boost your self-confidence you’re in charge of your emotions - you can do more. 

But there’s also your thoughts that 22 puts you in control of. Normally, we have a constant flow of thoughts for the mind to chew every minute. At 22, though, you [choose a few thoughts]({{ site.url }}/thoughts), say 3, and mull them serially. Over time, your self-regulation becomes so well-developed you’re no longer a slave to your own impulses. 

Still, you can restrict your 22 session to just one think and make it worth the time.

### Discipline
22 is a discipline. 

It builds discipline - defined as the ability to take action despite fights against by other desires. Really, discipline is akin to self-control, except the sense of deliberateness. Where self-control is about restraints, discipline is action - you go do it despite. This is how greatness is achieved. Leo Babauta of Zen Habits calls [discipline a superpower](//zenhabits.net/self-discipline/){:target="_blank" rel="noopener"}. I couldn't agree more. 

Several 22’s build up your superpower so much so that when you set goals, you go do it - drive or no drive. You just do it.

### Focus
22 would already have conditioned your mind into mastering focusing on one thing, think, or task at a time. Focus becomes second-nature, with which you can always do it to a finish and well that you must do. 

While I’m not a fan of productivity as today’s business world has defined it, focus is a key ingredient. You actually do get the few things done that ought to be done.

### Mindful
Remember during 22 you have a choice of what one, two, or three things to mull over? Bring your list - a set of positive thoughts or ‘visualization of the best outcomes' for worrisome ones. 

Still yet, you can better understand difficult situations or encountered challenges being mindful at 22. This transpires beyond the practice. So, you’re literally mindful all the time! You know where mindfulness brings you? Better, more sleep. Endurance. Self-control. Discipline. Focus. And one as if it caps them all... Well, it does. Joy.

### Joy
Some have [defined joy as](//blog.deliveringhappiness.com/blog/6-tips-to-help-you-reset-during-tough-times){:target="_blank" rel="noopener"} “living in the present trusting that tomorrow is not worth worrying about”. 

During 22 only those 22 minutes count. Not even the 23rd minute is worth worrying about. Then, as much as you do it and know you’ll do it, you have an entire day of joy - day after day. 

Joy can be chosen by you. It's satiation no matter what.

{% include components/break.html %}

Calm. Endurance. Self-control. Discipline. Focus. Mindfulness. Joy. 7. A perfect number as considered by many. But certainly there’s many more benefits of 22.