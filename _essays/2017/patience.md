---
title: 'Patience. Remember calm'
date: 2017-06-30
description: |-
    From getting provoked to screaming to fighting, I think there's certain extremes a calm person must avoid; patience is how they can.
---
Here’s how I’m making sense of patience as a virtue and [benefit of 22]({{ site.url }}/why) - bottom line? [Calm]({{ site.url }}/calm). Come with me. 

Would a calm person get provoked? Would a calm person flip out and scream because provoked? Would a calm person provoke? Cuss? Would a calm person fisticuff? Would a calm person do in haste? Anything. Would a calm person be outraged? Outrage? 

If you couldn’t provoke or be provoked how much more outrage? Would a calm person panic? 

More? Perhaps.

### To have patience
I believe if I keep asking these questions introspective, at some point, I would’ve so internalized the don’ts and dos of patience I could proclaim of me, “I’m a patient person - I got patience.” I’m calm. You?

> You ought to be calm, unruffled, untroubled, come what may.

So, "would a calm person do that?” Then, remember calm and you’ll be patient. Patience is thus a [benefit of 22]({{ site.url }}/benefits) - a result of remembering you ought to be calm, unruffled, untroubled, come what may.

### 7 don’ts of patience
1.  Provoke
2.  Be provoked
3.  Scream
4.  Cuss
5.  Fisticuff
6.  Haste
7.  Panic.

Is it possible to achieve your aims in situations without screaming, cussing, or haste? Without panic, outrage, or fisticuffs? Try the dos.

### 7 dos of patience
1.  Appeal to
2.  Bear
3.  Deep-breathe
4.  Relieve
5.  Yield
6.  Slow
7.  Relax.

Matching these, we would say:

1.  Don’t provoke people. Appeal to them. Or move on.
2.  Don’t get provoked. Bear up. Keep cool - it’ll be okay.
3.  Don’t scream. Take a deep, deep breath first and see. Deep-breathe.
4.  Don’t cuss (yourself or others). Relieve (yourself or others); make the problem seem not too serious. Easy.
5.  Don’t fight. You don’t need to. Yield as far as possible. If you must defend, perhaps escalate to the appropriate authority.
6.  Don’t hasten for the sake of it. In fact, don’t even be in a hurry to get or achieve anything. Go slow, deliberate, and your life will be funner.
7.  Don’t panic. Relax. Breathe. It’s perfect for your heart. Yet, it’ll all be okay.

### How to go about this?
Here’s what I would do if this essay were from someone else. I’d bookmark it or leave it open in a tab in my browser for sometime. Then read it at least once everyday until I would’ve remembered these 7 don’ts and dos of patience. 

Now, say I’m doing the same of my essay. Would a calm person…? You? 

Would you be patient today? Tomorrow? Always? Remember calm.