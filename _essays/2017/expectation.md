---
title: Sleep & expectation postponed
date: 2017-09-30
themes:
    - sleep better
description: |-
    I'm experimenting with how to have a 'desire unrealized' yet keep calm, sleep as long as I want to, be sated, and be happy.
footnotes: |-
    ### Mastering Sleep
    I seem to have survived EPS, especially since now I can easily let everything go and sleep. While we shouldn't be callous, we shouldn't let demands of the body affect our [equanimity]({{site.url}}/words/equanimity). I'm guarding my equanimity jealously as long as I'm conscious.
---
You can’t refute the wisdom of the Bible. When it says “expectation postponed makes the heart sick”, as in Proverbs 13:12, one area where that has the most impact is sleep. For me.

### My expectation postponed
I actually had [conquered insomnia]({{site.url}}/sleep) with 22. At least, that to a good deal degree. 

Well, until, O, where’s she - my woman? Because she ought to be beside me, right? So there’s an understandable demand for her. [For it]({{site.url}}/enjoy), if you will. 

To be clear, I’m doing rather poorly with sleep at the time of this writing. And, I’m thinking it’s because, while I’m trying to decide which woman to marry, I’ve “postponed” the desire for one until. So, what I’m experimenting with now is how do I live with the experience of having a strong demand, a ‘desire unrealized’, and yet keep calm, sleep, be sated, be happy? 

I do not have the answers.

### A few things I’m trying
While I do not have the answers, I've kept at these few things. My sleep as well as relaxation isn’t where it should be, really will be (being very deliberate here), but I sure am keeping calm with these.

1.  **Prayer.** Prayer for help and of gratitude. I don’t say much, I don’t even shut my eyes or prepare for it. I just utter “O, Jah, help me”, and “thank you.” Thank you!

2.  **Graticall.** That’s a word for grateful recall. I simply let my memory only bring back things I’m grateful for. If some not so savory thought arise, and they do do, I quickly move past it to another thanksworthy one. It’s a practice.

3.  **Sudden 22s.** I say sudden 22s because, usually, I’ll prepare to go to 22. To [do 22]({{site.url}}/why). But as soon as I notice the EPS coming, I just go 22. Because that works. It doesn’t get rid of the demand, what ever will? But it sure keeps me above it. 

    By the way, EPS stands for expectation postponed syndrome. Sure you'll know how to define that.

4.  **Apples.** It’s the one snack I don’t have a time for and I always have them handy, especially the green very watery ones.

5.  **Tea.** It’s how the flavor of my favorites make me feel. Earl Grey from Typhoo and Vanilla Tranquility from Ahmad. With apples and tea, I love the feeling of eating without getting filled, engaged with the process, keeping reasonably distracted from the demand, the other one.

6.  **Writing.** If it comes, I have to share something. This particular writing happened because EPS came.

7.  **Reading/Learning.** The question I ask is “what one thing can I put to use tomorrow?” That’s what I go to learn by reading or watching.

### ‘Desire realized’
My list above is certainly not 'the tree of life.' Never mind that it’s 7. But they sure help me feel happy and endure my expectation postponed and the EPS it brings with it, so until my 'desire for a woman is realized'.