---
title: No, one, or more thoughts?
editors_notes: |-
    Don't fail to read the update to this essay at the end of it. Because it better clarifies why you should focus on one thought or none during 22.
date: 2017-06-02
description: |-
    How much or many thoughts should you have during 22? I really encourage none or at most 1. But it's about your calm, not the number of thoughts.
footnotes: |-
    ### Updates
    The one thing I want to add here is that we should more often surrender at 22 than "interrupt the mind". Now see that, "interrupt the mind". You see, the mind never pauses, it's like the sun with many bodies orbiting around it endlessly. No sooner does it notice one orbit than another one zooms in and passes it, then another, and another...

    A thought only becomes a thing to the mind, thus consciousness, when we pause on it, when we interrupt the mind's constant receiving of inputs and begin working on the one thought with the brain. And we can do this many many times, which may lead to weariness, because there's even so much, a lot!

    So, more often than not, I'm doing nothing at 22 - no "conscious thinking", no interruption of the mind - or just one think, which I plan in advance, even so as not to just take whatever the mind throws at me, or, maybe, whatever is thrown at my mind.
---
Defining [22]({{site.url}}/what), I have said that you should have no thoughts at all or just one thought while at the practice. I’ve thought differently about this since. Here’s my current thinking.

### No thoughts
If knowledge of so-called blank meditation isn’t strange to you, I’m just as familiar with it. But, tell you what, I’ve never succeeded in “emptying my mind” of thoughts so that I doubt if it’s possible. Perhaps others can, but I cannot and will never attempt to again. 

So, in your 22, if you can go blank and it works for you, fine, go ahead. But I'm discounting the “no thought” aspect of 22.

### One thought
It’s only 22 minutes. Sometimes it may take longer to resolve a single thought. When [I conceived 22]({{site.url}}/mri) and restricted it to one thought, the purpose was not so much about the thought as was about focus. You see, too many thoughts and threads of thoughts distract the mind, disrupt calm, and can defeat the purpose of 22 - calm. 

Thus, if you could have just one think to focus on you would come to calm in the process. But what I’ve found at a couple 22’s is that I have too much minutes left after I’ve resolved the one thought I'd brought to the practice. I have had to have more than one thought.

### More than one thought in series
Another calming way to go is two or three thoughts. That is, if you must exceed one thought, then two, but no more than three. I have continued to try and would advise a regurgitation of thought. So if you stick to two or three, then you will come back to restudy the first or second just so you aren’t introducing just another thought brand new. 

What I’ve found also makes 22 worth the while is to have a preparation, like a [to-think list]({{site.url}}/thinklist) of two or three thoughts, then go about them in series as far as the minutes last. 

For me, 22 will continue to be about a thought or two. No more.