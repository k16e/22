---
title: How do you do 22?
date: 2017-06-14
description: |-
    As a very simple practice, there's but 5 things you need to do 22 - a timer, a bed/chair, a pillow, a ready mind and a ready body.
---
How do you do 22? Just do it! 

Well, as far as I’ve been doing it, I can say it takes a prepared mind. But I now always look forward to it - it’s become a pleasure my body seeks. The pleasure of calm. Good desire. 

To do 22 you need:

1.  A timer,
2.  A bed or a comfortable chair, (never do 22 standing!)
3.  A pillow with enough bounce to hold the head up slightly above the shoulders but enough sink to let it feel rested.
4.  A mind in airplane-mode, and
5.  A body nonchalant.

Once you have these 5 you’re ready. The rest is just getting into it.

### How I get into 22
I'll take a deep breath, most times a couple deep breaths, letting out all the yawns now, and speak calm to my mind. I'll say, “mind, suspend all other thoughts, except this or these,” the [thought]({{ site.url }}/thoughts) that I’ll mull over at 22.

Then I’ll put my phone in flight mode, launch the clock timer, and set the phone down beside me. Almost there. Breathe. 

I can do 22 wherever I can sit comfortably. But my favorite spot is my bed at home, free of external distractions. I’ll lie supine in the middle, legs splayed apart so each heel is at each edge, and hands brought together in a loose clasp behind my head. Next, another deep breath, then tap the Start button, and fly - 22.

### Your list for 22
1.  **Thoughts:** Decide which [one thought or no more than three]({{ site.url }}/thoughts/) thinks you’ll have for the session. One is most calming.
2.  **Pose:** Any pose will do so long as you’re comfortable - [22 isn’t yoga]({{ site.url }}/what/). Besides you don’t want irritation or strain, which is distracting and can abort the session.
3.  **Place:** Office, in transit, workplace, at home? Wherever you can be comfortable and can tune out distractions or not be disturbed by them.
4.  **State:** Should you sleep, stay awake, or go into trance? Entirely up to you. But you must close your eyes because [sights are distractions]({{ site.url }}/distractions).
5.  **Time:** Really? Of course, 22 minutes! But you can start with 2, then 3, 5, 10, and so on till you’re pleasured enough for more. Then 22, perhaps more!
6.  **Movements:** None! There’ll be several itches each as titillating as they come. Scratch none! Your [endurance will make you happy]({{ site.url }}/why).

### During 22
Be sure not to be stimulated into any physical movement by whatever you choose to do during 22. Physically moving any part of your body is distracting and may impede the mind's silence. The following things, though, shouldn't disturb your 22:

1.  Swallowing (your own saliva or phlegm, but deathly gently)
2.  Deep-breathing (as much as leave comfort without being the center of your 22. 22 is an exercise of calm, not a breathing one.)
3.  Yawning (if not too stretchy-physical, otherwise avoid yawning - you can, you should’ve had enough before.)
4.  Licking your lips. Yes, go ahead, lick ‘em, suck 'em in, and along with that, smile with your solemnity.
5.  Tongue-counting your teeth or traversing your teeth's backsides with the tongue. Do this if it helps your concentration and calm.
6.  ...

As I do 22 more I’ll have more thoughts to share how to make it effective. Sweet.