---
title: Negative emotions? No. Yes
date: 2017-07-30
description: Rather than suppressing negative emotions, here's 4 things I'm doing so as not to let them disrupt my calm.
---
Perhaps nothing else disturbs and disrupts [calm]({{site.url}}/calm) than negative emotions. Guilt, regrets, shame, dissatisfaction, depression, “I wish-ish”, sadness over poor performance, urges or cravings...they just sit in there and eat you away. 

Everyone suffers these emotions from time to time. I also am sure, like me, everyone tries to suppress them. What I’ve learned is to not suppress them.

### Do not suppress negative emotions
Rather than suppress them,

1.  Recognize that I’m sad or upset or guilty or regretful and accept the feelings however strong whenever I feel them, just accepting them as they come, letting myself experience them, inhale, hmmm, exhale, repeat.
2.  Assure myself that these emotions will pass, they’re just feelings, after all, it’s just a matter of time.
3.  Think ok, so I’m as horrible as I was back then, right, I sinned, I messed up, I gave a subpar presentation, yeah, I let my nerves take over… but so what? I’m a damn human!
4.  Yeah, so what, but why? Not why in order to judge myself for the goof or serious sin. But why or what led me to it? How could I forestall what things or actions led me to it? How could I do better?

My goal here is to shun whatever I’ve judged sinful so that its consequent negative emotions may be bye-bye. I would love that very much! And yet, if they come again?

> Sin needs your pity to perpetuate itself.

I need to know what could cause me to trip, to fall. Because I don’t want anything to disrupt my calm, my faith. Then take steps to avoid those. There again I need to know why I’m dissatisfied by letting the feelings run their course and by so doing find an answer.

### Handling a typical negative emotion
For example, to give a better public presentation without nerves getting in the way, I need to seek more opportunities to do so, I need to be free of ego - accepting that whatever happens to people can happen to me, without questioning. Who am I? I need to speak from that position of panic, knowing I have a damn good job to do!

### But as for sin
For me, beyond all else, a past sin or sins cause me more negative emotions. It’s because I’m very morally conscious but still remain in a human flesh. Still in my carnality. 

_On purpose, I’ve avoided defining sin here. But, I know my sins. Yet, what may be sinful to me may not matter to you. I do think, though, that if we have congruence in ethics we may also have congruence in what we judge sinful. Still, let it be as you judge._ 

I’ve found that sin itself is a trap - the more you find reasons to blame yourself for it, to keep regretting it, the less you find reasons to get out of it, the less reasons you find to get out of it, and the less you can get out of it. You go, “Arh, I’m just not good enough…I better be the way I am...” so you continue in it. It's the trap. 

So, if you’ve resolved to quit it, let that be what you think more about, not how weak you were to have relapsed. Because sin needs your pity to perpetuate itself. Then, you perpetuate negative emotions. You could lose your calm. Never let that happen!

### Some positive actions
Make a note of all the actions or thoughts that either cause or may cause you to feel these negative emotions and be deliberate about avoiding them. That is, the triggers, not the emotions themselves, which, do experience should they come. 

Try [the practices enumerated in this article](http://liveanddare.com/negative-emotions/){:target="_blank" rel="noopener"}. 

Notice when you’re triggered to these emotions, when they wave past you, and confidently say no. Try. It won’t be easy but have in mind the disturbance to your calm giving in will cause and say no. Keep trying until you get used to it. Don’t relish it. Don’t desire the desire. Don’t think O how I’m gonna enjoy it. Just let it go. 

Keep focus on what you’re about at present which is the good, high-consciousness stuff, hopefully, and persist in that. Don’t trip on guilt - you’re human, accept that goof, forgive yourself, understand why remorse, move on.

{% include components/break.html %}

To be sure, negative emotions will surface from time to time, you are human, don’t push back, accept them but don’t trigger them. Take positive and deliberate steps to stop those things that may cause them. 

Let calm be your control.