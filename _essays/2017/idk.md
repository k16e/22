---
title: I don't know
date: 2017-08-13
description: |-
    The idea of "I don't know" is to operate only from our area of excellence and never to presume to know what we do not, cannot, or should not know.
---
If your wristband or t-shirt or tea mug has the abbreviation "idk" printed on it or the full-form “I don’t know”, wouldn’t you be reminded always that that’s one of the calmest ways to live? 

I don’t know.

> Just say “I don’t know” and you’ll be fine.

I’ve found that not only does this answer let you off the hook, but it saves you a lot of stress and stomach sickness from fear of the consequence of goofing. You'd otherwise struggle to get your promise of knowing right for its sake or just to impress people. And, you’d likely goof! 

Whether born with it and/or have had to develop it through [practice over the course of life]({{site.url}}/middle), each of us has an area or a few areas of excellence. This is our “[circle of competence](https://www.farnamstreetblog.com/2013/12/mental-model-circle-of-competence/){:target="_blank" rel="noopener"}”, a term I learned studying [Farnam Street Latticework of Mental Models](https://www.farnamstreetblog.com/mental-models/){:target="_blank" rel="noopener"}. 

Problems from our circle of competence don’t stumble us. We may be challenged, but we always find the resources to deal with them. But we begin to crash and burn when we presumptuously stray off these bounds. That’s when our [calm]({{site.url}}/calm) gets messed up.

### idk and calm
Of course, knowing only too little can lead to your producing mediocre results. I’m not encouraging being so proud of _idk_ that you keep to a limited set of knowledge. Really, the whole circle of competence mental model thing is for you to continue to expand it over the course of life. But then to only operate from within it, not outside of it. Because as soon as you presumptuously operate outside its boundaries, you risk damage to your calm. 

In order to attract a girl to himself a boy claims he’s in Harvard. He wins a way with her. He even proves his “Harvardship” by acing a monumental research for her that she’d been assigned by her boss. But there’s a problem. 

He’s not in Harvard (or, really, any law school) and his won-over girlfriend’s boss finds that out. The girlfriend’s boss trashes his great research work and challenges his claim of Harvardship in his girlfriend’s face. If only the problem would end with just losing his new relationship, because the girl cuts him loose for good. 

Years later, there was this same woman at the door as prosecutor of a legal battle that was his first as junior partner at his undeserved law firm. He bolts upon sighting her, leaving a paralegal associate, who’s unfortunately caught in his fraud as his fiancée, to handle the case. 

He goes through a lot of pressure, gives his fiancée a share of some, almost loses the case, puts his firm through some expensive settlements, gets found out, only to grasp after yet more blackmails to bury his fraudulence. 

Did he have his calm? 

You may know the story - [Suits](https://en.wikipedia.org/wiki/Suits_(TV_series)){:target="_blank" rel="noopener"}. Did Mike Ross have his calm? 

### Say idk - avoid fear and pressure
You suffer fear from inside and pressure from outside when you operate outside your circle of competence. These two opposing forces can never let you have calm. As for fear,

1.  you’re terrified being exposed as a fraud and
2.  the ignominy that could come to you should you make a mess of doing well what you’re expected to do well because you said you could.

For pressure,

1.  it’s usually a thing that’d greatly impress people if you could do it, so people who want to be impressed will mount a lot of pressure on you to get it done, and
2.  they’ll, in turn, pressure you to come clean about who you really are the moment they notice you’re not the expert you claimed you are.

There’s no way on “God’s Green Earth” you can suffer such fear and pressure and maintain your calm, not even a false appearance of it. 

{% include components/break.html %}

I’d encourage you as an experiment - with or without something tangible - keep being reminded of your circle of competence and “never be afraid to say ‘I don’t know’. 

idk.