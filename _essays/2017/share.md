---
title: Want to share
date: 2017-11-19
description: |-
    Sharing is calming. Sure, this calm comes from sharing itself, but the willingness to share and an absence of fear to share both add up.
---
I have found that with a willingness to share whatever I got, the fear of fearing to share gone, I got more calm. So, I can conclude that sharing is calming. Thus, I want to share anything with anyone at anytime - it’s [happy]({{site.url}}/happy), God is happy, I’m happy.

> Whatever I have is for at least two.

This experiment as well as adjustments in personality has let me get more meaning out of a folk wisdom and culture I now fully recall and have imbibed.

### A history to share
In the community where I was born, the people, now including me, almost out of rote utter such expressions at any meal as

1.  Join me (or us),
2.  Wash your hands,
3.  Don’t just stand there looking,
4.  Here it is!
5.  Let’s do this together, 
6.  It's not sweet without you, and
7.  It’ll be enough for all of us.

I’ve loosely translated a few of these expressions as best as I can, but all are variations, personal interpretations, of the one thing - “join us”. Food is here, join us. You get the idea. Us. 

They understood that they could always join and be joined at it. I had no one to interview at the time of this writing, but I could induce during a few 22 on this one that this tradition goes back to a time in that community, Kaani, when all its members were so few they almost belonged to one parent whom they could all identify. 

This one parent desired their children to be fair to each other, be happy, a happy community, also enviable. So they started a sharing culture, so repeated, almost enforced, it became second-nature. Today, although no longer as widely practiced, even as Western-style [individualism](https://en.wikipedia.org/wiki/Individualism){:target="_blank" rel="noopener"} has eroded very much everything, this sharing culture has left me a scent of experiment - I got to share whatever I got.

### The fears got to go
In order to succeed at this experiment and vital practice, I’m letting go of the following fears:

1.  The pressure in the pit of my stomach when I imagine someone may want to join me at this morsel,
2.  The agitation - damn, it won’t be enough for me after I share it, whew,
3.  The fear I could even lose it altogether, this sharer, whoever they are, may just take everything, or deserve everything, and
4.  The interrupted enjoyment of the thing because fear.

### My justifications - perfecting sharing
The way I’m going about perfecting to share and sharing whatever I got for at least one more person is by holding on firmly to the [“abundance mentality”](https://www.stephencovey.com/7habits/7habits-habit4.php){:target="_blank" rel="noopener"}. Necessarily, I’ve broken this mentality down into 7 justifications:

1.  There’s an abundance of everything. This is so self-evident, don’t you think?
2.  I already have enough, if not more than enough. Really, God has put a little extra on top for me that I may give an other.
3.  Whatever little I got left after I have shared is all I needed in the first place. This, too, can be shared!
4.  If nothing is left after I’ve shared, then nothing was all I needed, at least at that particular time, that instance.
5.  Sharing is happifying - [it’s happy.]({{site.url}}/happy)
6.  God is happy - He [“loves a cheerful giver.”](https://www.jw.org/en/publications/bible/nwt/books/2-corinthians/9/#v47009007){:target="_blank" rel="noopener"}
7.  I’m happy - I got to be.

### How’s it been?
Not easy at all! 

I still sometimes grudge to think that I’m sharing something I worked so hard for with someone I perceive as lazy, careless, immoral, uncaring, unthankful or any one more from a thousand adjectives an unwillingness to share may bring to mind. 

A typical example is with my vegetables. 

If a colleague or neighbor were to pick or be given an apple from my stock, I sometimes get hung up on this feeling, “but I don’t get to have _their_ chicken!” “Why don’t they themselves buy apples, too!” Then I feel bad to share. It's sometimes so nasty you could tell from my comments - “You really want this?” 

Really? 

So, no, it’s not been easy at all. But, as an experiment, with constant practice, never quitting - it’s happy - I’ll master the need to share. And want to share. All the time.