---
title: MRI and the idea of 22
date: 2017-05-22
description: |-
    I could've bypassed MRI but then there'd be no 22. Imagine all the benefits that would've been lost! It's my favorite medical experience ever. 
---
"You should be out in an hour...if you remain still. It'll take longer if you move." 

"Really? Wait, was it the same for the little baby in its mother's arms — the ones that just got out?" 

"Well, we anesthetized him..." 

I was going to ask for the anesthesia option... 

"No, it's only for the little ones.” "Shall we?" She motioned me to the MRI unit.

### MRI doesn't matter
After so many years of believing I had cancer somewhere in my head or brain, I opted for MRI. 

That it was expensive doesn't matter. That it was "unnecessary" doesn't matter. Two major reasons the doctor had persuaded me to skip it. The doctor was quite sure I was fine, that my problem was merely mental, a feeling that had become "symptomatic." 

That the result came out "normal study," still doesn't matter. 

The result had left the doctor smiling, "so, you know what to do now?" I did. He'd had asked me to destress and I'd be fine. What he didn't know, though, which is what matters, is that, with the MRI, I'd found a perfect way to destress. All he thought was now I had the benefit of the doubt, I could at least outgrow the I-must-have-a-cancer fear. 

I got more.

### The MRI
I need not explain MRI to anyone who's been through this 'confined space', this narrow tube with its thunders and lion-roars and trains and a thousand crickets over the head. 

To the uninitiated though, just know I was laid on a narrow, stretcher-like bed, and rolled into a drum-like cover. The drum-like cover was then clamped about my neck so that my head was swallowed up inside such that I couldn't turn it around a single inch. The radiologist then drew my arms out and placed them on my sides and asked me to remain still. Silent. Quiet. Calm. 

I was free to open my eyes if I wanted to but it was best shut. I should be as still as in sleep and yet if I could remain awake better because then I could control body movements. Scratch no itch. She was going into her examination room, opposite but sealed off, would be seeing me through a monitor, alone within the unit for as long as the examination lasted — at most an hour, if I "complied", but cancelled and restarted if I moved. Instructions clear. 

My classic claustrophobia came in cascades. Sweating, too, in torrents. What would I do? Scream? "Get me out, get me out. Get me outta here?" Would I take those instructions? Who sent me? I should've listened to the doctor. I didn't need an MRI after all. No.

{% include components/break.html %}

### "We'll start all over again"
I must have slept off and moved. Because she came in, after what must have been my most enduring three quarters of an hour, and announced "cancelled". I moved. "We'll start all over again". 

What? God! This second time I was best off stiller or I'd end up wasting her entire day with repeated trials. She wouldn't take that. I had to grow up. 

{% include components/break.html %}

When she later returned, an hour plus later, I was, whew, relieved to hear, “done. You're good." She unclamped me and asked me to rise. I was numb from upper body through arms down into the toes. She held me up a few minutes until life-re-giving blood flowed through and brought feelings back. Then she led me out. Done. 

Had the cancer gone? 

It felt like. But I'd have to see the result of this excruciation first.

### The result and a new me
As I've already mentioned, the result came "normal study." Celebrate that. Because, look, you're normal — no cancer, nothing. Except that I decided I was definitely going to tell about this, I already had the MRI behind me. Whether I had cancer or not didn't matter again! Because a new realization had consumed me. 

Really, I, of all people, the antsiest human to ever live could stay still, silent, unmoving, and calm for more than an hour? The calm it brought to my head. I felt filtered. Refined. New. All sorts of good. I got home that day determined to MRI myself everyday. How to do it I wasn't sure.

{% include components/break.html %}

### The idea of 22
I think it was the next day, I returned from the office exhausted, pressurized, troubled, nerved, even stiffened as I'd be most all evening after work. Took a shower, felt a little better, then what? While I lay supine wondering, I thought MRI, 'pretend this is another MRI?'. 

I think I slept off and when I woke, the day's stress had all gone. It worked, but I slept. And I must have moved. It could've been the relief of sleep. Next time I'd set a timer to this. Because I wasn't going to move for as long as it lasted. 

How I flipped to 00 22 00 on my phone and took that to be the right duration and hit START I cannot tell, [I do not know]({{site.url}}/idk). 22 had just clicked as the right amount of time to MRI myself. That evening, I lay in what would become my favorite position for 22 — supine, hands brought in touch with each other above the head, legs splayed apart — and went motionless for 22 minutes. 

Every itch resolves itself. There were many — each as titillating. Yet, I endured. It was MRI after all. I was alone with my thoughts. But I had the option to choose which to focus on. I could hear my breath. See it come in and out my nostrils. Feel the gentle pulsing up-down-up-down my stomach. I took a deep breath once or twice. Swallowed a couple times. But I ensured whatever internal movement I allowed didn't make me shift physically or distract my thinking. 

While at it, I decided I was going to build a program called, well, "22", I was going to be saying "do 22", "just do 22 and you'll be alright", "you done 22 yet?" and so on. I'd look for opportunities to tell others about this. A way to master calm. I, people can be undisturbed no matter what... The alarm went.

### 22
You're probably reading me on [a blog I've named Calm essays]({{ site.url }}/calm-essays/) so that's one way I intend to bring this exercise of calm to you. I continue to do 22 everyday, sometimes twice a day, with [a lot of benefits]({{ site.url }}/benefits). 

My desire is to share these with you, to build and encourage a culture of calm, focus, purpose. For when you're 22, you're alone, out of trouble, and have the choice of what to focus on. 

I got several ideas on 22, which I'll share in more Calm essays. Stay tuned.