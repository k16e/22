---
title: Sleep is discipline
date: 2017-07-03
themes:
    - sleep better
description: |-
    I believe that a tranquil mind will fall asleep anytime. Or really, that you can always sleep if your mind is at ease.
footnotes: |-
    ### Getting physical
    I'm noticing something peculiar about "maintenance insomnia" in my case. It's as if 22 is a bit too passive to help me make enough melatonin to feel sleepy again and get back to it. 
    
    Increasingly, I'm finding that "getting physical" is helping - cooking a small meal, doing a bit of laundry, even scrubbing, yoga, sex, a bit of aerobics, or reading light - to calm the mind enough to let me sleep.
---
A tranquil mind keeps falling asleep anytime the body makes like it wants to. For example, if you lie, even if just your head over a desk, the mind yields, you sleep. 

So I’ve been convinced for sometime now. I’ve also become additional sure since I’ve been consistent with 22, speaking calm to my mind, reaching that inner conviction of calm that is [serenity]({{site.url}}/words/serenity). 

Sleep is indeed discipline. Yes, you can sleep whenever you want to! 

But people suffer insomnia, chronic insomnia, deep depression, and other sleep disorders. They want to sleep but cannot!

### I looked into sleep
Thus, I looked further into sleep because, yes, those are valid concerns. I wanted to be able to justify my own conclusions. 

Is 22 a panacea for sleep? Maybe not 22, but shouldn’t a tranquil mind fall asleep easily? And if tranquility of mind can be cultivated? Why not? 

Can 22 resolve insomnia? 

In my case, 22 has brought me sleep. Yes, I did suffer insomnia, always feeling I had cancer, which, thankfully led me to [discovering 22]({{ site.url }}/mri). But, let’s look at insomnia.

> Take a serene, unruffled repose for 22 minutes and see...

### Insomnia
Whether short- or long-term, insomnia may be caused by bad news, travel, loss, anxiety, depression, and/or some physical pain, say arthritis or back aches. I’m also aware that pregnancy with some women can impact sleep very much, leaving them with acute or chronic insomnia. 

For insomnia caused by factors of a physical nature, such as pain or pregnancy, it resolves if and when the pain is treated. It doesn’t take a specialist to make certain of that, does it? For the other factors of an emotional and/or mental nature, even without a sufficiency of experiments, I continue convinced that 22 can resolve it. 

What I’d advise if you’re reading me and suffer insomnia is [try 22]({{ site.url }}/how) and let’s see. Take that serene, unruffled repose for 22 minutes and see... Because [a serene mind]({{ site.url }}/calm) can receive bad news - of death, of accident - can lose money in a business transaction, in stocks, and otherwise, and yet remain [unruffled]({{site.url}}/words/unruffled). Thus, sleep will not be impacted. You choose not to worry. The situation wouldn’t be one of not being able to sleep. But, if you want to stay awake for some reasons, sure. Because you could if you wanted to. 

So, to answer my question, yes, 22 or calm or a tranquil mind - all but one thing - can resolve insomnia.

### Onset and maintenance insomnia?
I also suffered these other forms of insomnia. Onset being difficulty falling asleep at the proper time at night; and, maintenance meaning difficulty staying asleep or getting back to sleep if aroused. Try to go 22, don’t set alarm this time, try to repose at your chosen bedtime. 

Initially, this may be a pain, a struggle. But getting used to 22, you’ll find you always sleep once you want, especially when your brain finds this time to be your sleep cycle. 

For maintenance insomnia, try 22 whenever you wake and see if it puts you back to sleep. It may not the first few tries, but you keep at it, without letting it trouble you. Soon you find your serene mind easing in at will. Breathe. Enjoy.

### Some sleep disciplines
All things being equal, everyone should go to bed at 9pm. Or really, everyone should rise at 5am.

I know, utopian, right? The thing is, we all must establish the kind of nighttime routine that cues our body to sleep - a fixed bedtime is one such. And early enough to afford us a solid 6 hour restorative rest, without impinging on the next day’s wake cycle. 

Sleep.org recommends anywhere from 5 to 10 hours of sleep for people, depending on age, with older adults needing the shortest duration and infants the longest - up to 18 hours. 

Some lifestyle changes that can help sleep are shared [on this page](https://sleep.org/){:target="_blank" rel="noopener"}.

{% include components/break.html %}

So, whether you’re a knowledge worker in an office, a doctor working night shifts, or a police officer on the beat at night somewhere, each person must find what works for them when they must sleep, then discipline themselves to do so. 

You can sleep if you want to - it’s discipline. 

I do believe the body can make as much melatonin as each one needs for a balanced sleep-wake cycle if the mind is serene.