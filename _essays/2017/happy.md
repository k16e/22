---
title: Do happy
date: 2017-10-22
themes:
    - do happy
description: |-
    When what makes you happy is happy you’ve found lasting happiness. You can.
---
Do what makes you happy.

It’s everywhere. [On the web](//duckduckgo.com/?q=do+what+makes+you+happy&atb=v55-3_a&ia=web){:target="_blank" rel="noopener"}. Off the mouths of motivational speakers. On the streets. In books, on tapes, in films. Everywhere. Do what makes you happy or do what you love or do what you’re passionate about...

But, it’s bad advice!

The good advice is “do what’s happy”. Or just “do happy”. That’s what I’m admonishing.

> I now filter everything I do through the question "is it happy?"

But “do what makes you happy” is so popular, so well-advised. How can it be bad?

### Why "do what makes you happy" is popular
Whatever excites and satisfies our passion leaves us happy doing it. At least it leaves that good feeling to our senses we like to call “happy”. 

Consider such reasonings as these:
*   If it makes you happy, why you got happiness, you’re happy, keep at it.
*   If you’re doing what you love, you got skin in the game. Sure, then, you can endure in it despite trials, and trials always come.
*   True mastery comes to those who specialize; and it’s easy to specialize because it makes you happy, you love it, you’re passionate about it.
*   You have a sense of purpose generally when you do what you’re passionate about, what makes you happy, what you love.

All valid reasons to do what makes you happy, right? Right. For these reasons, it’s such popular advice. 

But, you can also make these same arguments for what is happy. So, there definitely is a problem with the former.

### The problem with the advice
What makes you happy may not be happy. 

The entire “do what makes you happy” admonition crumbles on this very important line. What makes you happy may not be happy. That passion of yours which you’re advised to be all about, to pursue vehemently, may not be happy. 

If it isn’t happy, then it’s bad. Because everyone should be happy. Which, really, is my point, in case you’re wondering “how can something make me happy and at the same time not be happy”.

### Why it’s bad advice
Your passion can harm others. Your passion may not benefit others. Yes, your passions may be completely selfish. Thus, your passion is unhappy. Bad. But it makes you happy! See why we should be admonished otherwise? 

What do you think a serial rapist is? Happy? Satisfied? Relieved? Well, yes. As long as he finds a prey. _You_ may be the only one calling him sadistic. Maybe _you_ just want him unhappy. But he’s doing what makes him happy, what he loves, his passion.

Are you a lover of cycling? Then you’re entertained to watch specialists of the sport, at the Olympics, for example. If you knew Lance Armstrong you sure felt happy watching him perform awesomely. Millions did. But how happy could you, or, really any supporter and follower of cycling, be to hear that this giant of cycling would be banned forever from the sport and stripped of all medals he'd won at the Tour de France?

### "Do what makes you happy" inspires bad practices
What the Armstrong case shows is “do what makes you happy” inspires and promotes bad practices. It happens in career, relationship, sports, etc. It’s a drug. You become so wrapped up in the idea you got to be #1, because, why, it makes you happy, it’s your passion, that it becomes an altar that takes your dignity and morality with it. And until something burns and bites, you don’t notice, you don’t care. See my point?

{% include components/break.html %}

### The good advice - do happy
Sweet people should rather advise us to do happy. 

To do what’s happy. 

That is, what leaves everyone, yes, _every numbered one_, happy at the end. 

To make what is happy what makes you happy or quit the pursuit altogether. You really need to make sure your passions and consequent actions won’t harm others or the environment, won’t leave others deprived, abandoned, and the like. It’s a question of good judgment. Perhaps no one should tell you what is good or bad. If you believe in God, you know he’s already done so and it’s as clear as day. 

Yet, people can tell you what is fair to all involved. And, no, this isn’t advise to be a “people-pleaser”. People will continue to be unjustifiably jealous at other’s achievements, which is them not doing happy, and there’s nothing you can do about that. 

But, overall, your actions shouldn’t in itself have any trace of hurtfulness.

This is such an ongoing struggle it’s important to filter everything we do through the one question: “is it happy?” Not “does it make me happy?” But, "is it happy?"

### The ultimate question
Is it happy? 

Pin that on your wall, make a wallpaper or screensaver of. Take it everywhere.

Incidentally, I now live and do everything entirely by how I answer that question. I'm not saying it's easy or that I'm awesome at it. Really, I'm struggling with it myself. But rather, that it's a worthy muscle, a good one to build. And it can be mastered. 

I want to smoke - is it happy? I want to have sex without the provision of marriage - is it happy? I want to "treat myself" because it's my birthday - is it happy? I want to dope in order to be #1 cyclist in the world - is it happy? 

It leaves you more mindful of consequences and a maker of good decisions, a taker of good actions. That’s how you’re well-advised. 

So, do you want to do what makes you happy? Is it happy? Indeed, when what makes you happy is happy you’ve found lasting happiness. You can.