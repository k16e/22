---
title: What is 22?
date: 2017-05-25
description: |-
    22 is "yogic" in that you suppress all activity of body, some of mind sometimes. But it's about literal calm, not "spiritual liberation."
footnotes: |-
    ### Some notes
    The mind may not necessarily [sleep]({{site.url}}/siesta/). But you sure rest totally because you don't interrupt it at this time. You can go into [catalepsy]({{site.url}}/words/catalepsy/) at 22, so that even at sleep, you're completely still - dead calm. When you wake up, nothing could be more refreshing.
---
22 is a yogic session where for 22 minutes you [repose]({{site.url}}/words/repose/) - sitting or lying - suppress all movements of the body, focus on [no thought or only one]({{site.url}}/thoughts/), and let yourself sleep off, go into a trance, or just remain still. 

I’ve introduced certain words in this definition than [originally]({{site.url}}/mri/). So, let’s unpack it for ourselves.

### Is 22 yoga?
No. 

When I say “yogic”, I use the reference loosely. I mean “it's like yoga and can calm you, can free you, can give you inner peace, can refresh you, reboot you." But 22 isn’t anything [like typical yoga](//www.webmd.com/fitness-exercise/a-z/yoga-workouts){:target="_blank" rel="noopener"}. It won’t stretch your muscles, won’t require you contort into some bizarre pose, may not be done standing, may burn no calories, won’t sculpt your body, nor relieve a sore back. At least, not necessarily. So, no, 22 isn’t yoga.

I don’t have a problem with yoga itself done for the sake of exercise, fun, health, and curves. I have a problem, though, with any religious or even spiritual attachments it may get. For example, I don’t believe there’s a self that is separate from the body and mind that needs to be liberated by some physical subjections.

I do encourage yoga. Sure, I won’t argue religion or dogma here. But 22 is nothing like the exercises and poses of yoga. There are poses or reposes in 22, but each one is only for the purpose of having enough physical comfort to sustain disciplined resting without the distractions that result from strain.

### Suppression of movements
When I started 22, I merely mimicked the experience of [the MRI I’d undergone.]({{site.url}}/mri) I wanted that refreshment, that reboot, that calm. I wanted MRI again, everyday, without the noise, of course! 

So, I thought if the MRI required no physical movement at all, movement must get in the way of clarification and relaxation of the mind. That’s why I suppressed movements down to even a scratch. 

During 22, you’re either zero-focused on one thought or none at all, because in the end you want mental clarity. Movements - scratching an itch, re-positioning an arm, turning the back or sides, shifting to ease strain, even yawning - can get in the way of that focus.

### Mental focus
I’ve determined that the ultimate purpose of 22 is calm. Mentally you’re calm and then you begin to radiate calm. 

A typical mind is overstimulated, made to zigzag a maze of countless thoughts, leaving the brain tired, stressed. These 22 minutes, anytime you take them, hopefully [several times a day]({{site.url}}/time), give your mind the freedom to rest. Because you pick only one thing and mull over. Or you pick nothing at all and just remain in a blank trance. 

> A tranquil mind supports healthy body functioning and enhances your well-being. 

So, in 22, your mental focus must be none or at most one.

### Sleep or trance
My first MRI session was cancelled because I’d moved. I'd slept off and shifted in sleep. In sleep, you aren’t quite in control of body movements. 

So, how come I encourage sleep during the practice? By the time I’d persisted at it a couple days, I noticed that the tranquilized mind kept falling asleep or remaining somewhat trancelike. [Sleep was irresistible]({{site.url}}/sleep). Forcing the mind to stay awake became an agitation that got in the way of my purpose.<sup>*</sup>

So now I just let it sleep if it wants to. I find that having stilled the body this long it remained in its repose even at sleep. I remember once I slept off. When I woke, I was feeling bloated-numb around where I folded my arms. I kept wondering how come this session was taking so long! I got tired and terminated it. As I looked at my phone, the alarm had gone 45 minutes over! So, I’d been at 22 for 67 minutes! And I was dead still and silent - unmoving - for as long. 

It’s doable. 

I recommend 22 daily or several times everyday. How often is convenient is another Skill. But you remain undistracted, unagitated, and undisturbed for as long as it lasts. 

Enjoy 22 today.