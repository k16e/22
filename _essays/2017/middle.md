---
title: The middle way
date: 2017-08-04
description: |-
    The middle way is a life of happiness and balance on the extremes of ecstacy and outrage, two emotions that're always in wait to be triggered.
---
I have got dealing better with my emotions. I’ve tagged how I'm doing this “the middle way” of the emotions based on something I've studied in the past in either or both of Stoicism and Buddhism. 

While the mind is open to impressions which can evoke the spectrum of emotions, you’re not so impressionable as not to have clear and rational judgment on what you feel and how you express it.

> Happiness is not a mood.

### The middle way
When [you’re calm]({{ site.url }}/patience), you’re imperturbable. You still have your emotions - you need them. But you’re not at their whims and caprices, whether of anger or of pleasure. You’re in charge. 

Feel pity, cry (to get rid of the pain or to make a connection), cackle to a good-natured joke, crack some yourself - for you, for them - eureka at epiphanous discoveries, punch the air a couple times, let the smile out, enjoy that good good jazz, what that caramel-edged sweet Scotch is doing on your tongue, to your senses, dance like no one’s watching, (the heck if somebody is watching) be absorbed in good drama. 

Go on. [Enjoy]({{ site.url }}/enjoy). The relief these bring. Have fun! But be aware that you still have you with the hold in the middle of these extremes - ecstasy or outrage. Seek the middle way. Enjoy but keep it in the middle. Be liberated from extremes. 

To be clear I make no reference to an “out-of-body-type liberation” as taught by "gurus". All of _you_ make up _you_, _you_ entirely. I’ve got this. 

It’s one thing to be upset and seek a redress; it’s another to be outraged, fly off the handle, break things. It’s one thing to grieve because she died, it’s another to gloom, refuse comfort. It’s one thing to eureka your code worked exactly, even better than anyone guessed, it’s another to act like you’re stuck on [MDMA](https://en.wikipedia.org/wiki/MDMA){:target="_blank" rel="noopener"}. It’s one thing [to panic before a large crowd]({{ site.url }}/negative), to have that unavoidable fight-or-flight response, it’s quite another to collapse and bolt. Or turn down an opportunity to be remembered. You’re in charge.

### For how long?
I'm concerned sometimes whether I’m becoming too edited, too programmed, too deliberate, too ruled, lack spontaneity or something. 

I’m not. Listen. 

It feels like one needs time, perhaps a long time, to be fully aware of themselves editing out all this bad stuff, these extremes, until they're just themselves sometime in future, going on, never stumbling. That’d be perfect! I’m not sure it's that way, though. I think it's a lifetime thing, where you just enjoy the process of growing up everyday, goofing sometimes, laughing at yourself, picking back up of where you left off, and just being stable. Going on. Growing. 

I think living the middle way will be daunting if it’s regarded as some preparation for a big one, for a perfect life. Which is why it is an experiment - I’ll just be aware of my reactions and actions and the stimuluses each day until I’m used to the preferred middle way of going about these. By which time I won’t be aware of so much anyway or be so aware I’m aware and be just fine. Still, when I stumble (and I could stumble), I’ll get back up, give myself permission to laugh at myself, and get back on to the middle.

### Staying in the middle
Of course, you can’t be too happy. Happiness is a state of being, a really who you are, or should be, but not necessarily a feeling you pick up now, sometimes drop, and sometimes hold up too much of. 

Happiness shouldn’t be a mood. But you could express too much happiness. You could over celebrate, make merry, party too loud, too long, or too much. No one should judge how much of your expression of happiness is extreme, too much. _You_ should. 

Observe how others, even you, are affected. Is your reveling encroaching upon your alone time? Do you find you’re bingeing or partying so long you’re not getting round to meditation, mindfulness, and spirituality? High-consciousness stuff? 22? Or even the required labor of the day? Work? Is your own “happiness” denying you of calm? Or of production? Then you know you're off the middle. 

[Do 22]({{ site.url }}/how) and get back to the middle. Stay stable, stay balanced. Stay in the middle. 

It’s similar with anger - the other extreme. And this is equally as observable as extreme cheer. Only, a lot more gets damaged. Which is one way you know it’s extreme. It’s the difference between laughing at someone or jeering at them. Do you find yourself breaking things or inclined to? 

Do you see yourself getting into a fight or thinking only a fight can settle it? Are you about to scream or already are? Are you yelling at everyone? Are you agitated? Would you rather kill yourself? Breathe! Breathe. 

You’re reacting, not acting, not responding. You’re taking action from a position of outrage and lack of judgment, not from that of justice. Granted, your action may be right. And, you could still take the same action. But better action, than re-action. It’s time you got back to the middle. 

{% include components/break.html %}

As you've seen, the middle way is a constant process of checking the self to see _you_’re in control - given to no extremes. Constantly seeking and staying in the middle way of the emotions in thoughts, in actions, in reactions, if any. I’ve got it and am at it. Try it, get used to it.