---
title: Nothing for the sake of it
date: 2017-07-20
description: |-
    Nature and reason are inherent in everything. So, nothing exists just for the sake of it. Nothing should be done for nothing.
---
Nature and reason are inherent in everything. Thus, nothing exists for the sake of it. Nothing happens just for the sake of it. Thus, nothing should be done for nothing or just for the sake of it.

> Do things for how they accord with nature, with life, with reason. Everything so.

### "Nothing for nothing" and 22
What’s this to [do with calm]({{ site.url }}/calm)? Thus with [22]({{ site.url }}/what)? 

If you’re serious about mastering calm try experimenting with doing nothing for nothing’s sake. Try having a reason for every action you take, every thought you let your mind mull. Try questioning your own motives, your own purposes, your own comforts. 

Why. 

The result is a more deliberate [life of enough](https://www.becomingminimalist.com/enough-is-enough/){:target="_blank" rel="noopener"}, of satisfaction, fewer to no worries, calm, happiness. Joy.

### It's practical
An example of this “nothing for nothing" or "nothing just for the sake of it” exercise is in food. A seemingly extreme but fitting example to go with food? Sex. I deliberately pair sex and food because they’re alike hot, enjoyable. 

_A caveat here, first. You can live and do awesome well without sex, unlike food. Which should also help in keeping the feeling in its place._ 

Think about sex same as you would food. You have to be hungry to eat. To enjoy the eating. Food. But you don’t eat because you’re hungry. Nor do you hunger just so you may eat. You eat because food “assists life according to nature.” 

Granted, people hunger that they may eat. Then they eat and eat and eat because they got gluttony. So they drink, too. So they go after sex, too. With gluttony. Everything with gluttony. You who're calm aren't such people. You aren't like that! You don’t have sex because you feel like, because you have a desire to do so. Heck, you don't have to have sex if you don't have to or shouldn't! You do need that feeling, that desire in order to enjoy sex where and when you should. 

> A desire for something, not for nothing. 

Really, that’s why we have desire at all. You have sex because it assists life according to nature; it builds intimacy; glues two for what it’s worth; and [its myriad other physical and mental benefits](http://articles.mercola.com/sites/articles/archive/2013/11/18/11-sex-health-benefits.aspx){:target="_blank" rel="noopener"}. Have sex so. 

This “nothing for the sake of it” is a gem that promotes [clear judgment]({{ site.url }}/why), peace, and good health. Start today to experiment with it and get used to it. 

Desire is good. But on purpose.