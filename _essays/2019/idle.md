---
title: Enjoy idle
date: 2019-07-13
themes:
    - lifestyle medicine
description: Idling can calm you, keep you at ease, and help you refresh. Enjoy it whenever you have the chance.
show_notes_form: true
---
Chances are you live alone, you’re yet to mate, you don’t wanna mate, your mate is away on a trip, you *are* away on a trip, or you can’t sleep. Or you really must be alone.

Whenever you find yourself like that, [*be* alone](/be), idle, enjoy the length of it.

### To enjoy idle
Reflect how it was with humans like us when they had no Netflix, no Twitter, no WhatsApp, no you-get-the-idea. No computer. No TV. No, *no* radio! Keep going back when they had just themselves and their conversations or, better, their aloneness.

Or maybe they had their external stimulation in sitting at nature and watching it stream on by. But what nature might you have at night alone and sleepless but the wandering of your mind? Hold that thought a moment.

We now have so much external stimulation, pleasures to fall back on that we forget to be truly alone, to enjoy idle, [which can be therapy](/themes/lifestyle-medicine).

So, when you find yourself in any one of those chances, take it, make it last.

The temptation is to wanna fill it with stuff - phone call, Netflixing, refreshing Twitter, swiping left on you might know what, porn, masturbation, emailing, getting to work on the computer, or a thousand other things we have these days to "make the time count".

To enjoy idle, [be idle when you are](/be) for as long as you can hold it - it can be a pleasure when you're eventually put to sleep by it. But if not, you can get back afterward to anyone of those stimulations, only then as a reward.

> Let your pleasures refresh, not reproach, you.

### Make it practical
1. **Go full DND** on your phone and computer so that even if you did check your device, you should see no notification that may suck you in. I don't have a clock on my wall because I hate the noise, so I'd check the time on my phone - my settings don't allow any notification on the locked screen.
2. **Get a "family & friends-only phone",** a basic feature-free phone for only phone calls. Only close friends and family should have this line. They should also know and respect the rules to use it - it must be urgent, they must really wanna talk to you, or be barred should they abuse it. Because you will let this one interrupt your aloneness. You only wanted to enjoy idleness, not hermitage.
3. **Think on paper** because you might not be able to fight the urge to see other things there should you wanna take notes on your phone or computer. Really, isn't it easier to avoid temptation than to fight it? Easier easier.
4. **Relish your reminiscences** as you [let your mind wander](/thoughts) where you might want it. Be mindful here that your wandering doesn't stimulate you and wake you too much up that you wanna terminate your idle and get back to the stimulations of this life. This reminiscing can be all the nature you got sometime, which do delight in it.
5. **Make your tea** or hot chocolate and take it without watching anything. Okay, watch the drink itself - the color, the movement in the cup or pot, feel the smell coming through, think how good it is, the taste, O how so good it is. The nuts or avocado you're gonna have with it, whatever else, look at those and nothing else.

### Therefore, idle
Idling can calm you, can keep you at ease, and help you refresh. It may be fuel for your next creative burst. Or, if and when it puts you to sleep, sleep, enjoy. It it happens on you, take it, or make it happen sometime.