---
title: Water? Wait, what time is it?
date: 2019-02-15
themes:
    - sleep better
description: |-
    Take all the water you want. Only be sure your last drink is 2 hours before bed time.
footnotes: |-
    ### References
    - [Nocturia or Frequent Urination at Night](https://www.sleepfoundation.org/articles/nocturia-or-frequent-urination-night)
    - [Melatonin and Sleep](https://www.sleepfoundation.org/articles/melatonin-and-sleep)
    - [Do I really need to drink 8 glasses of water a day?](https://www.onemedical.com/blog/live-well/daily-water-intake/)
---
Nocturia.

Big word, and I thought that would be my diagnosis if I did see a doctor. Because I was getting up to go up to 4 times between 9pm and 6am of a typical bedtime.

Of course, when you’re going that much, you’re either losing sleep by the same amount or, worse, all of sleep for the night. I was drinking too much - water.

So I started the “water hack” (ideally, “no-water hack”, naming is hard), and sleep improved a lot, and some nights, I haven’t gone at all and slept through. Thus, I’ll recommend you try the hack.

### The no-water hack
Yes, naming is hard, because this can again be called “intake hack” (O, oh, no-intake hack, there you go!). Anyway, whether water, tea, coffee, snacks, or food, stop intake of anything at least 2 hours before your preferred bedtime. No more intake till the next day.

Now be sure to drink as much water (_I mean water, not tea or coffee or beer or soda or juice or smoothie_) as your body needs during the day - between 9am and 7pm. Tea or coffee or beer or you get the idea should be seen as watery substances, not water - don’t let them count to your daily water need, but water alone. Thirst is your cue as to how much water you need, but be sure to drink at least 1 cup every hour. When your a-cup-an-hour adds to water from food and the watery substances, you should be getting as much as your body needs every day, and not worry about dehydration.

When I have met my daily water need between 9am and 7pm, I haven’t had an issue fasting 7pm to 9am, letting me sleep a lot better.

### A bit of science
As melatonin is produced and pumped into the bloodstream, body temperature lowers, alertness drops, and sleep insists it’s its time. This melatonin activity happens in a bell-shaped curve between 9pm and 9am, usually peaking around 1am/2am.

A full bladder that wants to empty will interrupt sleep and may inhibit release of melatonin, leaving you without sleep or poorly slept.

Try this no-water hack and let’s see if it helps. May you [sleep]({{site.url}}/sleep).