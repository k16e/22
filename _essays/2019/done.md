---
title: When it's done
date: 2019-01-01
description: |-
    Whatever you dislike or makes you hurt, whenever you get it or it happens to you, is done; what do you do when it's done?
themes:
    - stresscare
---
A tweet is done. A book done. A car done. A war over, done. A death done. What will you do about it?

Whatever you dislike or that makes you hurt, whenever you get it or it happens to you, is done; what will you do about it? What can you do with it?

What if you scroll on to a tweet that has a word you truly loathe? Will you utter, once again, “why's she always say these things” or option-pick “I don’t like this tweet” or block or mute or just unfollow her? All options are on the table, right?

What if in the book you’re reading, a print book - so it’s very much done - you come across several, yes, from beginning to end, several, it becomes clear to you either the author is ignorant or deliberately faulting, several occurrences of a “glaring misspeak”? Will you dump the book? Go tweet about the errors? Go rant about them with like-minds? Write the author immediately, admonishing or shooting her down? See past these errors, still finish the book and [be](/be)? Or dump it, dumping it and throwing it away? Again, you got all options.

Or, what about a car, say the new Lexus, you just hate the shape of? Why’d they do a new one anyways - the old was perfectly okay! Will you turn every hangout with your pals into a car talk just to have the opportunity to bash the new car? Or will you consider it done, let it be, let it go, and live like it never existed; no, even if your best friend chose no other car of the millions you prefer but this “lousy Lexus!”?

### What to do when it’s done
Because we should make it a habit to [never complain again]({{site.url}}/no-complaining/), we should learn these two skills to use when we encounter a situation we cannot or should not do anything about.

When it’s done,

1. Accept it and let it be, or
2. Abandon it and move on.

Let’s come back to the examples we started our narrative with.

1. For the tweet you hate, unfollow the account. Simple. You might tolerate them once or twice by unliking the tweet - letting Twitter do the work of filtering your timeline for you. But it’s best you follow only a few people you want to hear from and have peace. That’s a Twitter best-practice right there!

    Would you keep “friends” who make you hurt? Be sure, though, that the hurt, the loathing, is genuine.

2. You’re not obligated to read any book, or really any material, cover to cover. Besides, a print book is very much done. If you were to talk to its author, maybe she’d correct herself in a new book or a new edition of the current one? Or maybe she had made her choice. Complaining won’t change the content of this one. Throw it away. Let it go, forget it, and move on - look, there’s a million more.

3. If anything was done by the time you saw it it is a car. Even some design flaws can’t be fixed on a recall, so that the manufacturer would usually start a refresh sometimes earlier than scheduled. Your hated car, if it’s that bad, would go down antiquity as a flawed piece of machinery. It is done.

    When it zooms past, look the other way. Or look hard at it and nod and smile and proudly declare “you got no hold on me, motor!” You might eventually appreciate it, you never know. But whether or not you do, accept it, let it go - never, not for a single moment, ever complain about it. It is done.

### Everything is done
By the time it gets to you, everything is done. A piece of music, even an album of bad ones, is done. A government blunder, done. The noisy water dispenser you bought, done. A Netflix Original, done. Your new iPhone with a factory error you can't return, done - you might live with it. The App of the Day that didn’t live up to its promises, done. You name it, and it’s done.

Everything is done.

You can either accept it, live with it, let it go, or abandon it completely and live like it never existed. The goal is to [never complain again]({{site.url}}/no-complaining/), to [have true peace]({{site.url}}/be/), [live care free]({{site.url}}/words/carefree/).