---
title: How to learn better
date: 2019-02-03
themes:
    - increase focus
description: |-
    The recipe for learning anything is made of four ingredients. What're they? How can you use them?
---
We cannot learn everything. No, we shouldn't learn everything.

Even if we had eternity to live, we'd be surprised how new and complex what we thought we'd learned before was when we came back to it, because we would, because our memory can only hold as much.

But whatever we desire to learn, we _can and should_ learn it better and get mastery of it. But let our "whatever we desire to learn" be but a few - once for the fingers of one hand - in order to [focus]({{site.url}}/themes/increase-focus).

> Focus as well as its awareness is calming.

For your few learning desires, I got a recipe of four ingredients that can help you get better. But above all, you got to [let yourself be]({{site.url}}/be) in order for the recipe to work for you. Really, I learned of it when I was researching how to be, how to master staying in that calm be state mostly.

### Here we go
#### 1. Unlearn
To unlearn is to come to a new concept with a blank state of mind, humble as a child, believing the information you're now getting is correct. You have to deliberately keep any old memory or preconceived ideas from interfering with the new one. As you keep repeating the new, acknowledge its correctness, look at its practicality, you'll build a muscle of it.

Do you ever have to think before giving your name? Riding a bike (if you can)? Unlocking your phone? But then, what if you had to forget such a memory? Unlearning may be tough like that, but it is the first and foremost way to learn anything.

To unlearn:

1. Come in fresh, blank.
2. Repeat the new stuff a lot.
3. Try and not do or think of the old.
4. Be fond of the new.

You'll find that with modesty and calm you can unlearn in order to learn better.

#### 2. Zen
To zen is to be calmly attentive, as if nothing else but your present focus matters. Be fully present, 100% there where you are learning to be able to fully absorb whatever concepts are involved. 

To zen, you need to:

1. Accept that everything can wait and must wait (except [emergencies](https://www.theminimalists.com/emergencies/){:target="_blank" rel="noopener"}).
2. Time for at least an hour session with DND turned on, taking no external inputs except one that has to do with the current learning activity. This step is like [Pomodoro](https://francescocirillo.com/pages/pomodoro-technique){:target="_blank" rel="noopener"}.
3. Listen with your eyes, hear with your brain, the ears are only for fresh air. Woo-woo-sounding? Funny? Well, training your eyes on the object of attention (of learning) if not shutting them (still leaving them focused on the object even when shut or lost in space) is sure to get you understanding.
4. Refuse to be manipulated by distractions because they're always around you. Arm yourself with the trio of [tunnel vision](https://en.wikipedia.org/wiki/Tunnel_vision){:target="_blank" rel="noopener"}, [selective attention](https://www.verywellmind.com/what-is-selective-attention-2795022){:target="_blank" rel="noopener"}, and [banner blindness](https://www.nngroup.com/articles/banner-blindness-old-and-new-findings/){:target="_blank" rel="noopener"}.

#### 3. Translate
Translate, not interpret, what you’re learning to yourself. Put it in your own language. 

I say “not interpret” because interpretation gets meaning lost in translation, if you’ll excuse the pun. Remember you shouldn't let preconceived ideas becloud your appreciating the new learning? Well, if you try to interpret too soon, to make meaning, so to speak, you might get a point, but miss _the_ point. You get the idea.

Thing is, you might eventually need to interpret, but delay doing so until you've grokked the entire language of the narrative; until it's as easy as hearing it in your mother tongue, that is, the language of your own brain. Meaning comes.

To translate:

1. Try just swapping words in the sentence, replacing unfamiliar with familiar. This may be tough since we're eager to seek meaning. Only yield to meaning when it's all too obvious it's unavoidable.
2. Use the dictionary to help with replacing a simpler word for a harder one.
3. Repeat the edited sentence over now in language familiar to you.
4. Let meaning come effortlessly.

#### 4. Teach
This fourth and final ingredient in the recipe to learning better is a consolidation practice - you make it stick by sharing with at least one other person than you. To translate is to teach yourself; to teach others.

To teach:

1. Know you got something worthy to share.
2. Coach, facilitate - help people gain understanding, not necessarily dictate meaning to them.
3. Know when to stop instructing.

### To summarize
I needed to master how to [be]({{site.url}}/be). So, I thought deeply about and researched the benefits of just being. When I found that learning better was one such benefit, I asked myself how. How do you learn better, and how or why does that require you to be? Then I was able to drill down to the four ingredients of unlearn, zen, translate, and teach.

I believe if we bring these four things to any concept we can learn it and master it as quickly as its inherent in the thing. Notice I don't say "possible"? The goal is to learn better, not faster.