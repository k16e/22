---
title: Say a mantra, keep calm
date: 2019-05-28
description: |-
    Because of the mind, keeping calm can be difficult. But with a mantra you can.
themes:
    - stay centered
---
Do you try to keep calm, stay still, and find you just can’t, however hard you try? Well, you’re not alone.

The way of the mind.

Yeah.

Left on its own, the mind wants to be in a million places at once. It’s why keeping calm can be difficult. I find immediately kicking into [my mantra]({{ site.url }}/thinklist/mantra) and staying at it for as long as I got to be calm is a nifty way to bring calm on. It works.

When you want to sleep, before sleep comes, minutes before your exam paper starts, when you have to wait quietly and not reach for your phone,  really anytime you want to just enjoy some peace and quiet, some genuine pause - there’s the restless mind to wrestle with.

Easy.

Say your mantra, keep at it, and your mind will yield to this one thing. Then calm will come. Enjoy.