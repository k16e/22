---
title: Are you confirmed?
date: 2019-01-17
description: |-
    Denzel had two expectations to postpone - a car and a confirmation. How or what would you advise him?
themes:
    - stresscare
---
“Are you confirmed...” the human resource associate asked the distraught manager, who was now convinced something reeked of office politics or incompetence, and found the question shocking. 

The manager, Denzel, had applied for the official car which was due all managers. Approval of his application had delayed between his department admin and his boss, the head of the department. Toni, his boss, blamed the admin for not bringing it to her attention on time; the admin insisted that it was she, Toni, who hadn’t yet signed it. The admin admitted she’d lapsed other times and delayed submitting memos, but this time, she was “more than 100% sure” she placed it on Toni’s desk the moment she received it from Denzel; that she’d even followed with a verbal reminder the next day.

It’s worth noting that the car was only 50-percent free, as the prorated balance was deducted at source from the beneficiary’s monthly pay until cleared over a maximum 4-year tenure. But it was brand-new. It was 50-percent off, as settled by the company. So, Denzel found the deal juicy. He thought it would be like a free car since his pay was good enough, only 33% was being deducted, and he could endure the company 4 more years. Things could turn around, you know.

2 months later, it was still unclear to Denzel whether Toni had endorsed his application at all, which surprised him because she had earlier on appealed to him as well as other managers to accept the offer. He remembered when she had approached him privately to ask if he liked the options to be offered and why he should take it - it would reflect well on his new position as manager and the company’s brand. Denzel was about a month old as manager at the time his boss approached him about the car.

So what was happening now? 

It was also impossible to remind Toni on any reasonable frequency, since she would neither pick nor return her calls and text messages. This had since been the pattern, as if to show Denzel who was boss or what - whatever it was, it was not clear to him.

Denzel got a strange excuse when he finally confronted Toni about the delay: “KPMG flagged a lot of our practices, but we’ve resolved them now. See, latest this Wednesday you’ll hear from us,” she said. What could that mean? Other managers who applied during the same period had received their cars and were cruising it. One manager had even bashed hers; as Denzel reckoned how careful he’d be with his - “not a single scratch for as long as I use it, and it can be for life!” But, what did it even mean, “you’ll hear from us”? 

Incidentally, KPMG is the accounts auditing consultant of the company’s. But what business did Denzel have with that? He was just a middle-level manager going about his duties as best he could.

Well, a week passed after the promised Wednesday, when bewildered Denzel was in copy of an email from his boss to the human resource associate. She had asked the associate to ’sort out’ the situation. The department admin, who’s also the company’s procurement manager and executive assistant to Toni, was also in copy.

The associate never responded to this email. No one acknowledged it.

It was 2 days later, when Denzel followed up on the email with a message on Slack, the company’s team communication tool, that he was asked the shocking question.

‘Are you confirmed? Because I need to know that you’re confirmed on the company’s staff before I send order to Procurement to go ahead with your car.’

What?

How could he understand that - he was only 4 months short of 3 years with the company. He’d been promoted twice, the latest being his current position as manager of a 12-person unit, which he’d held for about 11 months at the time. Could all of those happen without a confirmation?

Was he confirmed? Denzel might wonder for a long time. Probably for as long as he chose to stay with the company. He thought that that would be an endurance race.

He reported the matter to his boss, and how the associate questioned his confirmation. Toni showed surprise and acted concerned. She apologized for the associate’s ‘ill-advised’ questions and comments and promised it would be actioned “today”. 

Aha, finally, Denzel would get his car - today!

{% include components/break.html %}

At the time of this telling, it was 8 weeks after the promised “today”. Nothing happened. No car. No confirmation. Nothing. Distraught Denzel would have two [expectations to postpone]({{site.url}}/expectation/).

Would he [postpone these expectations gracefully]({{site.url}}/gracefully/)? Or would he see that it was done, that is, the company, and do [what I suggest to do when it is done]({{site.url}}/done/) - accept the situation as is and let it and everyone be or abandon the company (his current job) and move on?

What about you, how would you respond? Or how or what would you advise Denzel?